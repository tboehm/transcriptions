\version "2.24.1"

\include "repeatBracket.ly"

\header {
  title = "Galactic Funk"
  subtitle = "From 'Cross Point' by Casiopea (1981)"
  composer = "Tetsuo Sakurai"
}

sectionA = \relative c, {
  %% e,8.\5 e'16\3 r b\4 cis8\4 fis8\3~ fis16\3 d16\3~ d8\3 e8\3 |
  e,8.\4 e'16\3 r b\4 cis8\4 fis8\3~ fis16\3 d16\4~ d8\4 e8\3 |
}

transitionToSectionB = \relative c, {
  e,8\5 e'\3 fis,\5 fis'\3 g,\5 g'\3 gis4\3 \bar "||"
}

sectionB = \relative c, {
  a8\5 r16 a16\5~ a16\5 a16\5 g8\5 a4\5 r4 |
  %% c8\4 c'\2 g\3 g\3 f\3 f\3 a,\4 a\4 |
  c8\4 c'\2 g\3 g'\1 f,\3 f'\1 a,,\4 a'\2 |
  ais,8\4 r16 ais16\4~ ais16\4 f16\5 g8\5 ais4\4 r4 |
  ees'8\3 ees'\1 ees,\3 ees'\1 d,\3 d'\1 d,\3 d'\1 |

  %% cis,8\4 r16 cis16\4~ cis16\4 a16\4 b8\4 cis4\4 r8 gis16 gis |
  cis,8\3 r16 cis16\3~ cis16 a16\4 b8\4 cis4\3 r8 gis16 gis |
  %% Outline of downbeats. There are eighth notes, too. I think it's octaves, but I can't
  %% I think this is octaves, but I can't play the low C#. I'll just play eighth notes.
  cis,8\5 cis' gis gis'\2 ais,\4 ais'\2 cis,\3 cis'\1 |
  fis,,8\5 r16 fis16\5~ fis16\5 e16\5 gis8\4 a4\4 r4 |
  b8\4 r16 b16\4~ b16\4 b16\4 a16\4 r16 r16 a8.\4~ a4~ |

  \time 2/4
  a2 |

  \time 4/4
}

sectionAInterruption = \relative c, {
  e,8. e'16\3 r b\4 cis8\4 fis8\3~ fis16\3 d16\3~ d16\3 r16 d'8\1~ |

  \time 2/4
  d16\1 e\1 a,\2 b\2 fis\3 b\2 e,\3 a\2~

  \time 4/4

  a2.\2 b,4\4 \glissando |
}

transitionToSolo = \relative c, {
  %% Transition to solo
  \time 12/8
  e,8. e'16 r c g8. g'16\2 r ees\3 bes8.\4 bes'16\2 r fis\3 cis8.\4 cis'16\2 r gis\3 |
  \time 4/4
  c\2 b\2 bes\2 a\2 aes\3 g\3 fis\3 f\3~ f4\3 r \bar "||"
}

solo = \relative c, {
  %% 16 bars of bass solo
  r2._\markup { "Bass solo" } a'4\2~ |
  a2. \times 2/3 {e'16 d b\2} a\2 a\2 |
  g4\3 r2 r8 d16\4 e\3 |
  fis16\3 a\2 b\2 d\1 e8.\1 d16\1 b\2 a\2 fis\3 e\3 d4\4 |
  \ottava #1
  r4 r8 fis'16 e fis16 fis e cis\2 b\2 g' e d\2 |
  r a' fis e\2 r b' gis fis\2 dis\2 r16 r d'16 b d b g\2 |
  e\3 a\2 \times 2/3 {g\2 e\3 d\3} b8\4 r r e'~ e4~ |
  e4~ e8
  %% Not really doable for me on a 4-string.
  %% e,,,16 fis a\4 b\4 d\3 e\3 g\2 a\2 g\2 gis\2 |
  %% Open strings work, though.
  \ottava #0
  %% e,,,16 fis a b d e g a g gis |
  e,,,16\5 fis\5 a\4 b\4 d\3 e\3 g\2 a\2 g\2 gis\2 |

  e,8. e16~ e e cis' (d\3) e,8. e16~ e e d'\3 (e\3~) |
  e16 d\3 (e8\3) a,16\4 (b\4) d\3 (e\3) g\2 (a\2) a\2 (b\2) d (e) a,\2 (b\2)
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  %% 8va, etc.
  \set Staff.ottavationMarkups = #ottavation-ordinals

  \key d \major
  \tempo 4 = 120

  \time 4/4

  \repeatBracket 24 {
    \sectionA
  }

  \transitionToSectionB

  \sectionB

  \repeatBracket 14 {
    \sectionA
  }

  \sectionAInterruption

  \repeatBracket 7 {
    \sectionA
  }

  \transitionToSectionB

  \sectionB

  \repeatBracket 16 {
    \sectionA
  }

  \transitionToSolo

  \solo

}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \transpose c c' {\Bass} >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \Bass >>
      }
    >>
    \layout { }
  }

  \score {
    \unfoldRepeats {
      <<
        \new Staff { \Bass }
      >>
    }
    \midi { }
  }
}
