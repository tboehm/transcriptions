\version "2.24.1"

\include "repeatBracket.ly"

\header {
  title = "Midnight Rendezvous"
  subtitle = "From 'Casiopea' by Casiopea (1979)"
  composer = "Tetsuo Sakurai"
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  \key g \major
  \tempo 4 = 92

  \time 4/4

  \partial 2
  r2 |

  %% Intro
  \repeatBracket 4 {
    e,4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 e,16\5 f\5 fis~\5 |
    %% fis4~ fis16 a ais b~ b4~ b16 dis e e,~ |
    fis4~\5 fis16\5 a\4 ais\4 b~\4 b4~\4 b16\4 dis\3 e\3 r |
  }

  e,2\4 \glissando e'\4 \glissando |
  a1\4 |

  %% Verse
  \repeatBracket 2 {
    e,4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 d\3 e\3 r |
    fis4\2 e16\3 b\4 r b\4~ b4\4 b16\4 e\3 r8 |
    e,4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 a,\4 b\4 r |
    %% not sure if beat 1 is cis or d
    d4\3 a16\4 ais\4 r b~\4 b4\4 b16\4 e\3 r8 |
  }

  c8. c16 g gis a\4 c~ c4 g16 g r b~ |
  b8. b16 fis g gis b~ b4 fis16 b r c~ |
  c8. c16 g gis a\4 c~ c4 g16 c r d\3~ |
  d4\3 cis16 c r b~ b4 e16 b r r |

  e,4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 e,16\5 f\5 fis~\5 |
  fis4~\5 fis16\5 a\4 ais\4 b~\4 b4~\4 b16\4 dis\3 e\3 r |
  e,4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 e,16\5 f\5 fis~\5 |
  fis8\5 ais'\2-. ais\2-. ais,16\4 b\4 g'8\3-. g\3-. d16\4 g\3 r e,\2~

  %% Verse (same as first time)
  \repeatBracket 2 {
    e4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 d\3 e\3 r |
    fis4\2 e16\3 b\4 r b\4~ b4\4 b16\4 e\3 r8 |
    e,4~\5 e8\5 b'\4 e16\3 r8 e16\3 r16 a,\4 b\4 r |
    %% not sure if beat 1 is cis or d
    d4\3 a16\4 ais\4 r b~\4 b4\4 b16\4 e\3 r8 |
  }

  c8. c16 g gis a\4 c~ c4 g16 g r b~ |
  b8. b16 fis g gis b~ b4 fis16 b r c~ |
  c8. c16 g gis a\4 c~ c4 g16 c r d\3~ |
  d8\3 a'\2 e' ees16 d~ d8 a\2 d,16\3 e\3 a\2 g\2~ |

  g4\2 d\3 g, fis |
  % glissando up to the g?
  g'8\4 r8 g,4\5 a\5 ais\5 |
  c8.\4 c16\4 g\5 gis\5 a\5 c16~\4 c4 fis,16\5 g\5 r b\4~ |
  b4 e8\3 g\2 f\3 e\3 d\3 c8\4~ |
  c\4 c'16\2 c\2 c,8\4 fis,\5 bes\5 d\4 g\3 gis'32 (g32 fis16 f8)


}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \transpose c c' {\Bass} >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \Bass >>
      }
    >>
  }

  \score {
    \unfoldRepeats {
      \new Staff { \Bass }
    }
    \midi { }
  }
}
