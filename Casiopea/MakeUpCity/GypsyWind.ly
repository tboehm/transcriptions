\version "2.24.1"

\include "repeatBracket.ly"

\header {
  title = "Gypsy Wind"
  subtitle = "From \"Make Up City\" by Casiopea (1980)"
  composer = "Tetsuo Sakurai"
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  %% 8va, etc.
  \set Staff.ottavationMarkups = #ottavation-ordinals

  \key a \major
  \tempo 4 = 112

  \time 4/4

  \partial 4
  r4 |

  \repeat volta 2 {
    b8.\4 \acciaccatura ais'16\2 \glissando b16\2~ b8. b,16\4~ b4\4 \acciaccatura a'16\2 \glissando b4\2 |
    b,4\4 \acciaccatura a'16\2 \glissando b8.\2 b,16~\4 b4. a16 aes\4 |

    g8.\4 \acciaccatura e'16\2 \glissando g16\2~ g8. a,16\4~ a4\4 \acciaccatura fis'16\2 \glissando a4\2 |
    b,4\4 \acciaccatura gis'16\2 \glissando b8.\2 c,16~\4 c2 |
  }

  \break

  cis8. fis,16 r gis r e'\3~ e4 \glissando b4 |
  a8.\4 ais16\4 r b\4 c\4 r e4\3 \glissando b4 |
  a8.\4 a16\4 r a\4 ais\4 r b4\4 \acciaccatura b16\4 fis'4\3 |
  b\2 gis,8.\5 g16~\5 g2\5 |

  \break

  cis8. fis,16 r gis r e'\3~ e4 \glissando b4 |
  a8.\4 a16\4 r a\4 b\4 r e4\3 \glissando b4 |
  a8.\4 a16\4 r a\4 gis\4 r gis4\4 dis'8.\3 gis16-.\2 |
  r dis16\3 gis,8\4~ gis4 fis8. fis16 r a ais b~ |

  \time 2/4
  b4 r8 d\3 |

  \time 4/4
  e8.\3 e16\3~ e8 c c8. fis16 d\3 d' des c |
  b8. b16~ b8 fis16 g\2 gis8.\2 gis16\2 d\3 gis\2 d' gis,\2 |
  g8.\2 g16\2 d8.\3 d16\3 g,8. g16 d'8\3 cis8 |
  c8 g'16\2 c r c r b r fis b,8 a8\4 d8\3 |

  e8.\3 e16\3~ e8 c c8. fis16 d'8 c |
  b8. b16~ b8 fis16 g\2 gis8.\2 gis16\2 d\3 gis\2 d' gis,\2 |
  g8.\2 g16\2 d8.\3 d16\3 g,8. g16 d'16\3 cis8 c16~ |

  \time 2/4
  c2 |

  \time 4/4

  %% Mostly a repeat, but the last measure is cut short to return to the intro.

  cis8. fis,16 r gis r e'\3~ e4 \glissando b4 |
  a8.\4 ais16\4 r b\4 c\4 r e4\3 \glissando b4 |
  a8.\4 a16\4 r a\4 ais\4 r b4\4 \acciaccatura b16\4 fis'4\3 |
  b\2 gis,8.\5 g16~\5 g2\5 |

  cis8. fis,16 r gis r e'\3~ e4 \glissando b4 |
  a8.\4 a16\4 r a\4 b\4 r e4\3 \glissando b4 |

  \time 2/4
  a8.\4 a16\4 r a\4 ais8\4 |

  \time 4/4

  b8.\4 \acciaccatura ais'16\2 \glissando b16\2~ b8. b,16\4~ b4\4 \acciaccatura a'16\2 \glissando b4\2 |
  b,4\4 \acciaccatura a'16\2 \glissando b8.\2 b,16~\4 b4. a16 aes\4 |

  g8.\4 \acciaccatura e'16\2 \glissando g16\2~ g8. a,16\4~ a4\4 \acciaccatura fis'16\2 \glissando a4\2 |
  b,4\4 \acciaccatura gis'16\2 \glissando b8.\2 c,16~\4 c2 |

  %% Solo section!

}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \transpose c c' {\Bass} >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \Bass >>
      }
    >>
  }
}

\book {
  \bookOutputSuffix "midi"
  \score {
    \unfoldRepeats {
      <<
        \new Staff { \Bass }
      >>
    }
    \midi { }
  }
}
