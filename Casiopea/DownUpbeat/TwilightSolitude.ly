\version "2.24.1"

\include "repeatBracket.ly"

\header {
  title = "Twilight Solitude"
  subtitle = "From 'Down Upbeat' by Casiopea (1984)"
  composer = "Tetsuo Sakurai"
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  %% 8va, etc.
  \set Staff.ottavationMarkups = #ottavation-ordinals

  %% \key d \major
  \tempo 4 = 56

  \time 4/4

  \partial 4

  r16 e8.\3 |
  c'4.\2 e,8\3 dis4.\3 c'8\2 |
  b4.\2 gis8\2 fis4.\3 e8\3 |
  b'4.\2 gis16\2 fis\3~ fis8 e\3 b\4 e\3 |
  fis8\3 b16\2 gis\2~ gis4~ gis8 r8 e4\3 |
  c'4.\2 e,8\3 dis4.\3 c'8\2 |
  b4.\2 gis8\2 fis4\3~ fis16 e\3 dis\3 e\3 |
  b'4.\2 gis16\2 fis\3~ fis8 e\3 b\4 e\3 |
  fis8\3 b16\2 gis16\2~ gis4~ gis8. b,16\4 gis'\2 e8.\3 |

  r1 |

  \tempo 4 = 112

  fis2\3 fis4\3 e8\3 fis8\3~ |
  fis4\3 e\3 b\4 e\3 |
  gis4\2 r gis\2 fis8\3 b\2~ |
  b2\2 gis8\2 e\3~ e4\3 |

  fis2\3 fis4\3 e8\3 fis8\3~ |
  fis4\3 e\3 b\4 gis'\2 |
  fis4\3 e8\3 fis\3~ fis2~ |
  fis4\3 r e8\3 e\3~ e4\3 |

}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \transpose c c' {\Bass} >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \Bass >>
      }
    >>
  }

  \score {
    \unfoldRepeats {
      <<
        \new Staff { \Bass }
      >>
    }
    \midi { }
  }
}
