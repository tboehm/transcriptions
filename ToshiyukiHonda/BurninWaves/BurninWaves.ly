\version "2.24.1"

\include "repeatBracket.ly"

\header {
  title = "Burnin' Waves"
  subtitle = "From \"Burnin' Waves\" by Toshiyuki Honda (1978)"
  composer = "Ken Wild"
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  %% 8va, etc.
  \set Staff.ottavationMarkups = #ottavation-ordinals

  % \key d \major
  \tempo 4 = 118

  \time 4/4

  \repeatBracket 4 {
    e,4 r16 e''16\2 d8\2 e8\2 d\2 b8.\3 r16 |
    e,,4 r16 e''16\2 d8\2 e8\2 e\2 g8.\1 r16 |
  }

  \break

  b,8\2 b,\4 b'\2 r16 fis\3 b8\2 r b\2 r16 fis\3 |
  b8\2 b,\4 b'\2 r16 fis\3 b8\2 b\2 fis\3 b,\4 |
  b'8\2 b,\4 b'\2 r16 fis\3 b8\2 b,\4 b'\2 r16 fis\3 |
  b8\2 b\2 fis\3 b,\4 \acciaccatura d'16 e4 \acciaccatura a,16\2 b4\2 |

  \break

  \repeatBracket 8 {
    e,4.\3 b'8\2 fis' b,\2 d16 e r8 |
    % e,8.\3 a16\2 r8 b8\2 r8. d,16\3~ d8 fis\3 |
    e,8.\3 a16\2 r8 b8\2 r8 d,8\3~ d8 fis\3 |
    e4.\3 b'8\2 fis' b,\2 d16 e r8 |
    e,8.\3 a16\2 r8 b8\2 \acciaccatura d16 e4 \acciaccatura a,16\2 b4\2 |
  }

  \break

  d,4.\3 d16\3 d\3 r8. c16~ c4~ |
  %% Not what's played, but I like it:
  %% g8 c, e g c b~ b4 |
  c8 c4 c4 c4 c8 |
  d4.\3 d16\3 d\3 r8. fis16~ fis4 |
  fis8 fis4 fis4 fis4 fis8 |

  \break
  
  e8.\3 e16\3~ e4 r8. gis16\2~ gis r gis8\2 |
  a8.\2 a16\2~ a4 r8. b16\2~ b r b8\2  |
  cis8. dis16~ dis8 r ees,8.\3 f16\3~ f8 r |
  r d'8 b8.\2 a16~\2 a8 fis\3~ fis16 e8.\3 \bar "||"

  \break

  d8\3 d\3 r d\3 r d\3 r d\3 |
  r8 d\3 r d\3 d\3 d\3 d4\3 |

  b'8\2 b,\4 b'\2 r16 fis\3 b8\2 r b\2 r16 fis\3 |
  b8\2 b,\4 b'\2 r16 fis\3 b8\2 b\2 fis\3 b,\4 |

  e4.\3 b'8\2 fis' b,\2 d16 e r8 |
  e,8.\3 a16\2 r8 b8\2 r8 d,8\3~ d8 fis\3 |
  e4.\3 b'8\2 fis' b,\2 d16 e r8 |
  e,8.\3 a16\2 r8 b8\2 r8 d,8\3~ d4 |

  \break

  \repeatBracket 4 {
    e4.\3 b'8\2 fis' b,\2 d16 e r8 |
    % e,8.\3 a16\2 r8 b8\2 r8. d,16\3~ d8 fis\3 |
    e,8.\3 a16\2 r8 b8\2 r8 d,8\3~ d8 fis\3 |
    e4.\3 b'8\2 fis' b,\2 d16 e r8 |
    e,8.\3 a16\2 r8 b8\2 \acciaccatura d16 e4 \acciaccatura a,16\2 b4\2 |
  }

  \break

  e,4.\3 e'4 e,\3 e'8 |
  e,4.\3 e'4 e,\3 e'8 |
  d,8\3 d\3 d\3 d\3 d\3 d\3 d16\3 d8.\3 |
  r8 b\4~ b2.\4 |

  \break

  \repeatBracket 3 {
    e,8 e r8. d'16\3 (e8-.\3) r16 c'16 (d8-.) r |
    e,,8 e r8. e''16 (d8-.) r16 d r d (e8-.) |
  }

  e,,8 e r8. d'16\3 (e8-.\3) r16 c'16 (d8-.) r |
  e,,8 e r8. e''16 (d8-.) d8 d16 (e8.) |

  %% 8 more bars like the previous 8. Then:

  d,4.\3 d16\3 d\3 r8. e16~\3 e4~\3 |
  e8\3 e4\3 e4\3 e4\3 e8\3 |
  ees4.\3 ees16\3 ees\3 r8. d16~\3 d4\3 |
  d8\3 d4\3 d4\3 d4\3 d8\3 |

  c'8\2 c,\4 c'\2 r16 g\3 c8\2 r c\2 r16 g\3 |
  c8\2 c,\4 c'\2 r16 g\3 c8\2 c\2 g\3 c\2 |
  b8\2 b,\4 b'\2 r16 fis\3 b8\2 r b\2 r16 fis\3 |
  b8\2 b,\4 b'\2 r16 fis\3 b8\2 b\2 fis\3 b,\4 |

  \bar "||"

  \break

  e8\3 e'16 b\2 d (e) r8 d,8\3 d8\3 r16 d8.\3 |
  e8\3 e'16 b\2 d (e) r8 d,8\3 d8\3 r16 d8.\3 |
  e8\3 e'16 b\2 d (e) r8 d,8\3 d8\3 r16 d8.\3 |
  e8\3 r16 b'\2 d e r8 dis,16\3 (e\3 dis\3~) dis r d\3 e8\3 |

  \break

  e8\3 r16 b'\2 d e r8 d,8\3 d8\3 r16 d8.\3 |
  e8\3 r16 b'\2 d e r8 d,8\3 d8\3 r16 d8.\3 |
  e8\3 r16 b'\2 d e r8 d,8\3 d8\3 r16 d8.\3 |
  e8\3 r16 b'\2 d e r8 e,16\3 d\3 r8 d16\3 e\3 r8 |

  \break

  c4. c8~ c2~ |
  c1 |
  f4. f,8 f2~ |
  f1 \bar "||"

  \break

  %% 3:19 in the track

  %% horns enter third time
  \repeatBracket 6 {
    d'16\3 e\3 g8\2 r4 a8\2 [r16 b\2~] b8 g8\2 |
    d16\3 dis\3 e8\3 r4 d8\3 d\3 e4\3 |
  }

  d16\3 e\3 r g\2 r4 a8\2 [r16 b\2~] b8 g8\2 |
  d16\3 dis\3 e8\3 r8. d,16 e8\5 r16 d'\3 e8\3 r |

  %% left over right before 114 in the Logic track
  d16\3 e\3 r g\2 r4 a8\2 [r16 b\2~] b8 g8\2 |
  d16\3 dis\3 e8\3 r16 e,8. e4 r |

  d'16\3 e\3 r g\2 r4 a8\2 [r16 b\2~] b8 g8\2 |
  % d16\3 dis\3 e8\3 r16 [e\3 r e,\5] d8 d'\3 a\4 b\4 |
  d16\3 dis\3 e8\3 r16 e\3 r e,\5 d8 d'\3 a\4 b\4 |
  b8\4 b\4 r16 g'16\2 a8\2 c c a\2 g\2 |
  b,8\4 b\4 r16 b8.\4 b8\4 b\4 r4 |

  d4.\3 d16\3 d\3 r8 e8\3~ e4~ |
  e1\3 |
  d4.\3 d16\3 d\3 r8 fis8\2~ fis4 |
  fis,8 [fis~] fis [fis] fis2 |

  %% Same as measure 15
  e'8.\3 e16\3~ e4 r8. gis16\2~ gis r gis8\2 |
  a8.\2 a16\2~ a4 r8. b16\2~ b r b8\2  |
  cis8. dis16~ dis8 r ees,8.\3 f16\3~ f8 r |
  r d'8 b8.\2 a16~\2 a8 fis\3~ fis16 e8.\3 \bar "||"

  d8\3 d\3 r d\3 r d\3 r d\3 |
  r8 d\3 r d\3 d\3 d\3 d4\3 |
  b'8\2 b,\4 b'\2 r16 fis\3 b8\2 b,\4 b'\2 r16 fis\3 |
  b8\2 b,\4 b'\2 r16 fis\3 b8\2 b\2 fis\3 b,\4 |

  \repeat volta 2 {
    e4.\3 b'8\2 fis' b,\2 d16 e r8 |
    e,8.\3 a16\2 r8 b8\2 r8 d,8\3~ d8 fis\3 |
  }

  \repeat volta 2 {
    e4.\3 b'8\2 fis' b,\2 d16 e r8 |
    e,8.\3 a16\2 r8 b8\2 r8 d,8\3~ d8 fis\3 |
    e4.\3 b'8\2 fis' b,\2 d16 e r8 |
    e,8.\3 a16\2 r8 b8\2 \acciaccatura d16 e4 \acciaccatura a,16\2 b4\2 |
  }

  e,4.\3 e'4 e,\3 e'8 |
  e,4.\3 e'4 e,\3 e'8 |
  d,8\3 d\3 d\3 d\3 d\3 d\3 d16\3 d8.\3 |
  r8 gis,\4~ gis2.\4 |
  
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \transpose c c' {\Bass} >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \Bass >>
      }
    >>
  }
}

\book {
  \bookOutputSuffix "midi"
  \score {
    \unfoldRepeats {
      <<
        \new Staff { \Bass }
      >>
    }
    \midi { }
  }
}
