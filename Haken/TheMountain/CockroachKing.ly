\version "2.24.1"

\include "repeatBracket.ly"

\header {
  title = "Cockroach King"
  subtitle = "From 'The Mountain' by Haken (2013)"
  composer = "Thomas MacLean"
  %% Based on tabs from https://www.songsterr.com/a/wsa/haken-cockroach-king-bass-tab-s392493
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  %% 8va, etc.
  \set Staff.ottavationMarkups = #ottavation-ordinals

  \key b \major
  \tempo 4 = 85

  \time 2/4

  r2 |

  \time 4/4

  %% Doubtful on the rhythm here. Bars 1 and 2 are the same as 3 and 4.
  b,4 b'8.\4 b16\4 \glissando < \deadNote e, b >8 b8~ b16 b'16\4~ b8 \glissando |
  < \deadNote e, b >4 b'8.\4 b16\4 \glissando < \deadNote e, b >16 b8 b16 \times 2/3 { b'8\4 b\4 b\4 } |
  b,4 b'8.\4 b16\4 \glissando < \deadNote e, b >8 b8~ b16 b'16\4~ b8 \glissando |
  < \deadNote e, b >4 b'8.\4 b16\4 \glissando < \deadNote e, b >16 b8 b16 \times 2/3 { b'8\4 b\4 b\4 } |

  %% Verse 1
  R1 * 8 |

  b'2.\2 fis4\3 |
  f2.\3 e4\3 |
  ees2.\3 d,4 |
  cis1~ |
  cis8 cis cis cis cis cis cis cis |
  cis8 cis cis cis cis cis cis cis |

  %% Verse 2
  b'8\4 r d\4 r 

}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \transpose c c' {\Bass} >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \Bass >>
      }
    >>
  }

  \score {
    \unfoldRepeats {
      <<
        \new Staff { \Bass }
      >>
    }
    \midi { }
  }
}