# Safe Make options
SHELL := sh
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c  # "strict" bash mode
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --silent

# will echo the value of variable $%
print-%  : ; @printf '%b' "$* = $($*)\n"

.DEFAULT_GOAL := all

LY_FILES := $(shell find . -name "*.ly" | grep -Ev "lilypond/")
PDF_FILES := $(LY_FILES:.ly=.pdf)
MIDI_FILES := $(LY_FILES:.ly=.midi)

IPATH := $(shell realpath ./lilypond/)

IFLAGS := $(patsubst %,--include=%,$(subst :, ,$(IPATH)))

define compile_ly
	lilypond $(IFLAGS) -o $(shell dirname $@) --pdf $^
endef

# TODO: Add a way to only create PDFs or only create MIDIs.
%.pdf: %.ly ; $(compile_ly)
%.midi: %.ly ; $(compile_ly)

.PHONY: all
all: $(PDF_FILES)

.PHONY: clean
clean:
	rm -f $(PDF_FILES)
	rm -f $(MIDI_FILES)
