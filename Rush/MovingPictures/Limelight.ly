\version "2.24.1"

%% Based on tabs from Peter Seckel, Mario D'Alessio, and Sean Jones via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/mp-limelight.btab

\include "repeatBracket.ly"

\header {
  title = "Limelight"
  subtitle = "From 'Moving Pictures by Rush (1981)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  \key a \major
  \tempo 4 = 144

  \absolute {

    \time 4/4
    \repeatBracket 6 { r1 | }
    r4 a,8\3 a,8\3 r4 b'4\1 \glissando \hideNotes \grace { g16\1 } \unHideNotes |

    \time 7/4
    \repeatBracket 4 { b,8\3 fis8\2 b8\1 a,4\3 e8\2 a8\1 e,4\4 fis,8\4 gis,8\4 a,8\3 cis8\3 a,8\3 | }

    \time 6/4
    b,2.\3 \glissando gis2.\2 |
    r4 fis4\2 cis8\3 fis8\2 r8 e4\2 e8\2 cis8\3 e16\2 e16\2 |
    fis8\2 r8 fis4\2 cis8\3 fis8\2 r8 e4\2 cis8\3 e8\2 e16\2 \glissando fis16\2 |
    b,2.\3 gis,2.\4 |

    \time 4/4
    fis,4\4 fis,4\4 r4. b,8\3~ |

    \time 7/4
    \repeatBracket 2 { b,8\3 fis8\2 b8\1 a,4\3 e8\2 a8\1 e,4\4 fis,8\4 gis,8\4 a,8\3 cis8\3 a,8\3 | }

    \time 6/4
    b,2.\3 gis2.\2 |
    r4 fis4\2 cis8\3 fis8\2 r8 e4\2 e8\2 cis8\3 e16\2 e16\2 |
    fis8\2 fis,8\4 fis4\2 cis8\3 fis8\2 r8 e4\2 cis8\3 e8\2 fis8\2 |
    b,2.\3 gis,2\4 gis4\2 \glissando \hideNotes \grace { d16\2 } \unHideNotes |

    \time 4/4
    fis,4\4 fis4\2 r4. b,8\3~ |

    \time 7/4
    \repeatBracket 2 { b,8\3 fis8\2 b8\1 a,4\3 e8\2 a8\1 e,4\4 fis,8\4 gis,8\4 a,8\3 cis8\3 a,8\3 | }

    \time 3/4
    gis,2\4~ gis,8\4 fis,8\4 |
    e,2.\4 |
    fis,2.\4 |
    gis,8\4 gis,4.\4 gis,8\4 fis,8\4 |
    e,4.\4 e4\2 b,8\3 |
    fis,4.\4 e,4\4 fis,8\4 |
    gis,8\4 b,8\3 cis16\3 (d16\3) cis4\3 b,8\3 |

    \time 4/4
    e,8\4 e,8\4 r8 e,8\4 e,8\4 e,8\4 r8 e,16\4 e,16\4 |
    fis,8\4 fis,8\4 r8 fis,8\4 fis,8\4 e,8\4 fis,8\4 g,8\4 |
    gis,8\4 gis,8\4 r8 gis,8\4 gis,8\4 gis,8\4 gis,8\4 gis,8\4 |
    e,8\4 e,8\4 r8 e,8\4 e,8\4 e,8\4 r8 e,8\4 |
    fis,8\4 fis,8\4 r8 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    gis,8\4 gis,8\4 r8 gis,8\4 r8 gis,8\4 gis,8\4 fis,8\4 |
    e,8\4 e,8\4 e8\2 e,8\4 e,8\4 e,8\4 e8\2 b,8\3 |
    fis,8\4 fis,8\4 r8 fis,8\4 cis8\3 b,8\3 ais,8\3 fis,8\4 |

    \time 3/4
    gis,2\4~ gis,8\4 gis,8\4 |
    e,4.\4 e4\2 b,8\3 |
    fis,2\4 cis4\3 |

    \time 7/4
    b,8\3 fis8\2 b8\1 a,4\3 e8\2 a8\1 e,4\4 fis,8\4 gis,8\4 a,8\3 cis8\3 a,8\3 |
    b,8\3 b,8\3 r8 a,8\3 r8 a,8\3 r8 e,4\4 fis,8\4 gis,8\4 a,8\3 \glissando cis8\3 a,8\3 |

    \time 6/4
    b,2.\3 gis2.\2 |
    r4 fis4\2 cis8\3 fis8\2 r8 e4\2 e8\2 cis8\3 e16\2 e16\2 |
    fis8\2 fis,8\4 fis4\2 cis8\3 fis8\2 r8 e4\2 cis8\3 e8\2 fis8\2 |
    b,2.\3 gis,2.\4 |

    \time 4/4
    fis,4\4 fis4\2 r4. b,8\3~ |

    \time 7/4
    b,8\3 fis8\2 b8\1 a,4\3 e8\2 a8\1 e,4\4 fis,8\4 gis,8\4 a,8\3 cis8\3 a,8\3 |

    \time 6/4
    b2.\1 gis2.\2 |
    fis4\2 fis4\2 cis8\3 fis8\2 r8 e4\2 e8\2 cis8\3 e16\2 e16\2 |
    fis8\2 fis,8\4 fis4\2 cis8\3 fis8\2 r8 e4\2 cis8\3 e8\2 fis8\2 |
    b,2\3~ b,8\3 cis8\3 gis,4.\4 dis4\3 gis,8\4 |

    \time 4/4
    fis,4\4 fis4\2 r4. b,8\3~ |

    \time 7/4
    b,8\3 fis8\2 b8\1 a,4\3 e8\2 a8\1 e,4\4 fis,8\4 gis,8\4 a,8\3 b,16\3 cis16\3 a,8\3 |

    \time 3/4
    gis,2\4~ gis,8\4 fis,8\4 |
    e,2.\4 |
    fis,2.\4 |
    gis,8\4 gis,4.\4 gis,8\4 fis,8\4 |
    e,4.\4 e4\2 b,8\3 |
    fis,4.\4 e,4\4 fis,8\4 |
    gis,8\4 b,8\3 cis16\3 (d16\3) cis4\3 b,8\3 |

    \time 4/4
    e,8\4 e,8\4 r8 e,8\4 e,8\4 e,8\4 r8 e,16\4 e,16\4 |
    fis,8\4 fis,8\4 r8 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    gis,8\4 gis,8\4 r8 gis,8\4 r8 gis,8\4 gis,8\4 fis,8\4 |
    e,8\4 e,8\4 r8 e,8\4 e,8\4 e,8\4 e,8\4 e,8\4 |
    fis,8\4 fis,8\4 r8 fis,8\4 fis,8\4 fis,8\4 fis,8\4 fis,8\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 gis,8\4 |
    e,8\4 e,8\4 e8\2 e,8\4 e,8\4 e8\2 e,8\4 b,8\3 |
    fis,8\4 cis8\3 fis,8\4 cis4\3 b,8\3 ais,8\3 fis,8\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 gis,8\4 |
    gis,4\4 r2. |

    \time 3/4
    gis,2.\4 |
    e,2.\4 |
    fis,2.\4 |
    gis,8\4 gis,8\4~ gis,2\4 |
    gis,2.\4 |
    e,2.\4 |
    fis,2.\4 |
    gis,8\4 b,8\3 cis16\3 (d16\3) cis4\3 b,8\3 |
    gis,4.\4 b,8\3 gis,8\4 fis,8\4 |
    e,8\4 e,8\4 e,8\4 e4\2 b,8\3 |
    fis,16\4 fis,16\4 cis8\3 fis,8\4 b,4\3 ais,8\3 |
    gis,8\4 fis,4\4 fis,8\4 gis,8\4 b,8\3 |
    gis,4.\4 b,8\3 ais,8\3 fis,8\4 |
    e,8\4 e8\2 b,8\3 \glissando fis4\2 e8\2 |
    fis,8\4 cis8\3 b,8\3 ais,8\3 b,8\3 cis16\3 e,16\4 |
    gis,8\4 fis,8\4 gis,8\4 b,8\3 fis,16\4 fis,8.\4 |
    gis,4.\4 b,8\3 gis,8\4 fis,8\4 |
    e,4.\4 e8\2 b,8\3 e8\2 |
    fis,4.\4 fis,8\4 e,8\4 fis,8\4 |
    gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 |
    gis,8\4 gis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 |
    e,8\4 e,8\4 e,8\4 e8\2 \glissando fis8\2 e8\2 |
    fis,8\4 cis8\3 b,8\3 ais,8\3 b,8\3 cis8\3 |
    gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 |

    gis,8\4_\markup "Rhythm guitar enters" gis,8\4 gis,8\4 b,4\3 cis8\3 |
    e,8\4 e,8\4 e,8\4 e4\2 b,8\3 |
    fis,8\4 fis,8\4 fis,8\4 e,4\4 fis,8\4 |
    gis,8\4 b,8\3 \glissando d8\3 cis8\3 b,8\3 gis,8\4 |
    e,8\4 e,8\4 e,8\4 e4\2 b,8\3 |
    fis,8\4 fis,8\4 fis,8\4 e,4\4 fis,8\4 |

    gis,8\4_\markup "Living in..." gis,8\4 gis,8\4 b,4\3 gis,8\4 |
    e,8\4 e,8\4 e,8\4 e4\2 b,16\3 e,16\4 |
    fis,8\4 fis,8\4 fis,8\4 e,4\4 fis,8\4 |
    gis,8\4 b,8\3 cis16\3 (d16\3) cis8\3 b,8\3 gis,8\4 |
    e,8\4 e,8\4 e,8\4 e4\2 b,8\3 |
    fis,8\4 fis,8\4 fis,8\4 e,4\4 fis,8\4 |
    gis,8\4 gis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 |

    \time 4/4
    e,8\4 e,8\4 r8 e,8\4 e,8\4 e,8\4 r8 e,8\4 |
    fis,8\4 fis,8\4 r8 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    gis,8\4 gis,8\4 r8 gis,8\4 gis,8\4 gis,8\4 gis,8\4 fis,8\4 |
    e,8\4 e,8\4 r8 e,8\4 e,8\4 e,8\4 r8 e,8\4 |
    fis,8\4 fis,8\4 r8 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 gis,8\4 |
    e,8\4 e,8\4 e8\2 e,8\4 e,8\4 e,8\4 e8\2 b,8\3 |
    fis,8\4 cis8\3 fis,8\4 fis,8\4 cis8\3 b,8\3 ais,8\3 fis,8\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 gis,8\4 |
    e,8\4 e4\2 e,8\4 e,8\4 e8\2 b,8\3 e8\2 |
    fis,8\4 cis4\3 fis,8\4 cis8\3 b,8\3 ais,8\3 fis,8\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 gis,8\4 fis,8\4 gis,8\4 |
    e,8\4 e8\2 b,8\3 e,8\4 e8\2 b,8\3 e,8\4 e8\2 |
    fis,8\4 fis,8\4 cis8\3 fis,8\4 cis8\3 b,8\3 ais,8\3 fis,16\4 e,16\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 cis16\3 (d16\3) cis8\3 b,8\3 |
    e,8\4 e,8\4 e8\2 e,8\4 e,8\4 e,8\4 e8\2 b,8\3 |
    fis,8\4 cis8\3 fis,8\4 fis,8\4 cis8\3 b,8\3 ais,8\3 fis,8\4 |
    gis,8\4 gis,8\4 fis,8\4 gis,8\4 b,8\3 cis16\3 (d16\3) cis8\3 b,8\3 |

    e,8\4 e,2.\4~ e,8\4 |
    fis,8\4 fis,2.\4~ fis,8\4 |
    gis,8\4 dis8\3 ais8\2 fis8\2 gis8\2 ais8\2 \glissando gis,4\4~ |
    gis,1\4 \fermata |
    gis,4\4 \glissando \hideNotes \grace { e,16\4 } \unHideNotes r2. |

  }
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    \unfoldRepeats {
      <<
        \new Staff { \transpose c c, {\Bass} }
      >>
    }
    \midi { }
  }
}
