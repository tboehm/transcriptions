\version "2.24.1"

%% Based on tabs from Dave Whitehill, Don Dianetti, and Sean Jones via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/mp-red_barchetta.btab

\include "repeatBracket.ly"

\header {
  title = "Red Barchetta"
  subtitle = "From 'Moving Pictures' by Rush (1981)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  \key g \major
  \tempo 4 = 144

  \absolute {

    r1 |

    \time 4/4
    a1\2~ |
    a2\2 r2 |
    fis1\2~ |
    fis2\2 r2 |
    g1\2~ |
    g2\2 r2 |
    d1\3~ |
    d2.\3 r4 |
    d'16\1 e'8.\1~ e'2.\1~ |
    e'2\1~ e'8\1 fis'8\1 r8 e'8\1 |
    cis'1\1~ |
    cis'2.\1~ cis'8\1 cis'16\1 cis'16\1 |
    d'2\1~ d'8\1 cis'4.\1 |
    b2.\2 b4\2 |
    a8\2 a8\2 a8\2 a8\2 d8\3 d8\3 d8\3 d8\3 |
    fis8\2 fis8\2 e8\3 e8\3 g8\2 g8\2 fis8\2 fis8\2 |
    a,4\3 a,8\3 a8\2~ a8\2 a8\2~ a8\2 a8\2 |
    a,4\3 a,8\3 a8\2~ a8\2 a8\2 e8\3 a8\2 |
    fis4.\2 fis8\2~ fis8\2 fis8\2~ fis8\2 e8\3 |
    d4\3 e8\3 fis8\2~ fis8\2 fis8\2 e8\3 fis8\2 |
    g4\2 d8\3 g8\2~ g8\2 g8\2~ g8\2 d8\3 |
    fis4.\2 g8\2~ g8\2 fis8\2 e4\3 |
    d4.\3 d'8\1~ d'8\1 d'8\1 d'4\1 |
    cis'8\1 e'8\1~ e'8\1 d'8\1~ d'8\1 cis'8\1 b4\2 |

    a1\2~ |
    a1\2 |
    fis1\2~ |
    fis1\2 |
    g1\2~ |
    g1\2 |
    a1\2~ |
    a1\2 |

    a8\2 a2.\2 a8\2~ |
    % a8\2 a8\2~ a4\2~ a4\2~ a8\2 a8\2 |
    a1\2 |
    fis8\2 fis2.\2 fis8\2~ |
    % fis8\2 fis8\2~ fis4\2~ fis4\2~ fis8\2 fis8\2 |
    fis1\2 |
    g8\2 g2.\2 g8\2~ |
    % g8\2 g8\2~ g4\2~ g4\2~ g8\2 g8\2 |
    g1\2 |
    d8\3 d8\3 r2 d8\3 d8\3~ |
    d8\3 d8\3 d4\3 d8\3 d8\3 cis8\3 b,8\4 |

    a,4\3 r8 g,8\4~ g,8\4 e,8\4 c8\3 b,8\3 |
    a,4\3 r8 g,8\4~ g,8\4 e,8\4~ e,8\4 c8\3 |
    c8\3 d8\3~ d8\3 e8\3 d4\3 c8\3 b,8\3 |
    a,4\3 r8 g,8\4~ g,8\4 e,8\4 c8\3 b,8\3 |
    a,8\3 b,8\3 c8\3 g,8\4~ g,8\4 e,8\4 c8\3 b,8\3 |
    a,4\3 r8 g,8\4~ g,8\4 e,8\4~ e,8\4 c8\3 |
    c8\3 d8\3~ d8\3 e8\3 d4\3 g,4\4 |

    a,4\3 g,4\4 d4\3 g,4\4 |
    a,4\3 g,8\4 d8\3~ d8\3 d8\3 g,4\4 |
    a,4\3 g,4\4 d4\3 g,8\4 r8 |
    g8\2 a8\2~ a8\2 b8\2 a8\2 r8 g,4\4 |
    a,4\3 g,4\4 d4\3 g,4\4 |
    a,4\3 g,8\4 d8\3~ d8\3 d8\3 g,4\4 |
    a,8\3 a,8\3 g,4\4 d4\3 g,8\4 r8 |
    g8\2 a8\2~ a8\2 g8\2 a8\2 r8 g,4\4 |
    a,4\3 g,4\4 d4\3 g,4\4 |
    a,4\3 g,8\4 d8\3~ d8\3 d8\3 g,4\4 |
    a,4\3 g,4\4 d4\3 g,8\4 r8 |
    g8\2 a8\2~ a8\2 g8\2 a4\2 e4\3 |
    a,4.\3 g,4.\4 c4\3 |
    a,4.\3 g,4.\4 c8\3 b,8\3 |
    a,4.\3 g,8\4~ g,2\4 |
    c8\3 d8\3 e4\3 d4\3 c8\3 b,8\3 |
    a,4.\3 g,4.\4 c4\3 |
    a,4.\3 g,4.\4 c4\3 |

    \time 3/4
    a,4.\3 g,4.\4 |

    \time 4/4
    c1\3~ |
    c1\3 |
    a4.\2 a8\2~ a8\2 a8\2~ a8\2 a8\2 |
    a,8\3 a,8\3 e8\3 a8\2~ a8\2 a8\2 e8\3 a8\2 |
    fis4.\2 fis8\2~ fis8\2 fis8\2~ fis8\2 e8\3 |
    d8\3 e8\3 fis8\2 fis8\2~ fis8\2 fis8\2 e8\3 fis8\2 |
    g4.\2 g8\2~ g8\2 g8\2~ g8\2 d8\3 |
    fis8\2 d8\3 fis8\2 g8\2~ g8\2 g8\2 fis8\2 e8\3 |
    d8\3 d8\3 d8\3 d'8\1~ d'8\1 d'8\1~ d'8\1 a8\2 |
    cis'8\1 e'8\1~ e'8\1 d'8\1~ d'8\1 cis'8\1 b8\2 d'8\1 |
    a,4.\3 a8\2~ a8\2 a8\2~ a8\2 e8\3 |
    a,8\3 a,8\3 a,8\3 a8\2~ a8\2 a8\2 e8\3 a8\2 |
    fis4.\2 fis8\2~ fis8\2 fis8\2~ fis8\2 e8\3 |
    d8\3 d8\3 e8\3 fis8\2~ fis8\2 fis8\2 e8\3 fis8\2 |
    g8\2 g8\2 r8 g8\2~ g8\2 fis8\2~ fis8\2 fis8\2~ |
    fis8\2 e8\3~ e8\3 e8\3~ e8\3 fis8\2 e8\3 dis8\3 |
    r8 a8\2 d8\3 d'8\1~ d'8\1 a8\2 d8\3 d'8\1~ |
    d'8\1 a8\2 d8\3 d8\3 d8\3 d8\3 d8\3 a,8\4 |
    e,1\4~ |
    e,1\4 |
    b8\2 d8\2 d8\2 d8\2 d8\2 b8\2 d8\2 cis'8\2 |
    d8\2 d8\2 d8\2 d8\2 d8\2 cis'8\2 d8\2 b8\2 |
    d8\2 d8\2 d8\2 d8\2 b8\2 cis'8\2 d8\2 d'8\2 |

    \time 2/4
    d8\2 cis'8\2 d8\2 b8\2 |

    \time 4/4
    d8\2 d8\2 d8\2 d8\2 d8\2 b8\2 d8\2 cis'8\2 |
    d8\2 d8\2 d8\2 d8\2 d8\2 cis'8\2 d8\2 b8\2 |
    d8\2 d8\2 d8\2 d8\2 b8\2 cis'8\2 d8\2 d'8\2 |

    \time 2/4
    d8\2 cis'8\2 d8\2 b8\2 |

    \time 4/4
    d8\2 d8\2 d8\2 d8\2 d8\2 b8\2 d8\2 cis'8\2 |
    d8\2 d8\2 d8\2 d8\2 d8\2 cis'8\2 d8\2 b8\2 |
    d8\2 d8\2 d8\2 d8\2 b8\2 b8\2 cis'8\2 d'8\2 |

    \time 2/4
    d8\2 cis'8\2 d8\2 b8\2 |

    \time 4/4
    d8\2 d8\2 d8\2 d8\2 d8\2 b8\2 d8\2 cis'8\2 |
    d8\2 d8\2 d8\2 d8\2 d8\2 cis'8\2 d8\2 b8\2 |
    d8\2 d8\2 d8\2 d8\2 b8\2 b8\2 cis'8\2 d'8\2~ |
    d'4.\2 cis'4\2 b8\2 a8\2 e8\3 |

    %% TODO: Continue making rhythms more readable
    ais,4.\3 a,4\3 a,8\3 f,8\4 a,8\3 |
    ais,8\3 f,8\4 ais,8\3 a,4\3 a,8\3 f,8\4 a,8\3 |
    cis8\3 gis,8\4 cis8\3 c4\3 c8\3 gis,8\4 c16\3 c16\3 |
    cis8\3 gis,8\4 cis8\3 c4\3 gis8\2 c4\3 |
    ais,4.\3 a,4\3 f,4\4 a,8\3 |
    ais,8\3 f,8\4 ais,8\3 a,4\3 f,8\4 f,8\4 dis8\3 |
    cis8\3 gis,8\4 cis8\3 c4\3 gis,8\4 gis,8\4 c8\3 |
    cis8\3 gis,8\4 cis8\3 c4\3 gis,8\4 c8\3 e8\3 |
    e8\3 b,8\4 e8\3 dis4\3 b8\2 dis8\3 b8\2 |
    e8\3 b,8\4 e8\3 dis4\3 b,8\4 dis8\3 d8\3 |
    g8\2 d8\3 g8\2 fis4\2 fis8\2 g8\2 a8\2 |
    g8\2 g8\2 a8\2 a8\2 b8\1 a8\2 g8\2 fis8\2 |

    \time 7/4
    a,4\3 a,8\3 a4\2 a4\2 e8\3 a16\2 a16\2 e8\3 a8\2 e8\3 a8\2 e8\3 |
    fis4\2 r8 fis4\2 fis4\2 e8\3 fis16\2 fis16\2 e8\3 fis8\2 d8\3 fis8\2 d8\3 |
    g4\2 r8 g4\2 g4\2 d8\3 g8\2 a8\2 g8\2 fis8\2 g8\2 fis8\2 |
    d8\3 a8\2 d8\3 d'4\1 d8\3 a8\2 d8\3 d'8\1 cis'8\1 d'8\1 e'8\1 d'8\1 cis'8\1 |
    a,4\3 a,8\3 a4\2 a4\2 e8\3 a8\2 b8\2 a8\2 b8\2 a8\2 e8\3 |
    fis8\3 fis8\3 r8 fis4\3 fis4\3 e8\3 fis8\3 e8\3 fis8\3 e8\3 fis8\3 d8\3 |
    g8\2 g8\2 r8 g4\2 fis8\2 r8 fis4\2 g8\2 fis8\2 g8\2 \times 2/3 {fis16\2} \times 2/3 {g16\2} \times 2/3 {fis16\2} e8\3 |

    \time 4/4
    d8\3 d8\3 a8\2 d8\3 d'8\1 d8\3 a8\2 d8\3 |
    a8\2 g8\2 fis8\2 e8\3 d8\3 cis8\3 b,8\4 gis,8\4 |

    \time 7/4
    a,4\3 g,4\4 d4\3 g,4\4 a,4\3 g,8\4 d4\3 d8\3 |
    a,4\3 g,4\4 d4\3 g,4\4 a,4\3 g,8\4 d4\3 d8\3 |

    \time 6/4
    a,4\3 g,4\4 d4\3 g,8\4 r8 g8\2 a4\2 g8\2 |

    \time 4/4
    a,4.\3 a4\2 a4.\2 |
    a,4\3 a,8\3 a4\2 a8\2 e8\3 a8\2 |
    fis4\2 r8 fis4\2 fis4\2 e8\3 |
    d8\3 d8\3 e8\3 fis4\2 fis8\2 e8\3 fis8\2 |
    g8\2 g8\2 r8 g4\2 g4\2 d8\3 |
    g8\2 g8\2 d8\3 g4\2 g8\2 fis8\2 e8\3 |
    d8\3 d8\3 d8\3 a4\2 d4\3 d8\3 |
    a8\2 g8\2 fis8\2 g8\2 a8\2 g8\2 fis8\2 e8\3 |
    a,4\3 a,8\3 a4\2 a4\2 a8\2 |
    a,4\3 a,8\3 a4\2 e8\3 a16\2 a16\2 e8\3 |
    fis4\2 r8 fis4\2 fis4\2 e8\3 |
    d8\3 d8\3 e8\3 fis4\2 fis8\2 e8\3 fis8\2 |
    g8\2 g4\2 g4\2 g4\2 g8\2~ |
    g8\2 g4\2 g4\2 g8\2 fis8\2 e8\3 |
    a8\2 d8\3 d'8\1 d8\3 a8\2 d8\3 d'8\1 d8\3 |
    d16\3 d16\3 d8\3 d16\3 d16\3 d8\3 d16\3 d16\3 d8\3 c8\3 b,8\3 |
    a,4\3 r8 g,4\4 e,8\4 c8\3 b,8\3 |
    a,4\3 r8 g,4\4 e,4\4 c8\3 |
    c8\3 d8\3 e4\3 d4\3 c8\3 b,8\3 |
    a,4\3 r8 g,4\4 e,8\4 c8\3 b,8\3 |
    a,8\3 b,8\3 c8\3 g,4\4 e,8\4 c8\3 b,8\3 |
    c8\3 d4\3 c8\3 d4\3 g,4\4 |
    a,4\3 g,4\4 d4\3 g,4\4 |
    a,8\3 a,8\3 g,8\4 d4\3 d8\3 g,4\4 |
    a,4\3 g,4\4 d4\3 g,8\4 r8 |
    g8\2 a4\2 b8\2 a8\2 r8 g,4\4 |
    a,4\3 g,4\4 d4\3 g,4\4 |
    a,8\3 a,8\3 g,8\4 d4\3 d8\3 g,4\4 |
    a,8\3 a,8\3 g,4\4 d4\3 g,8\4 r8 |
    g8\2 a4\2 g8\2 a8\2 g,16\4 g,16\4 g,4\4 |
    a,4\3 g,4\4 d4\3 g,4\4 |
    a,8\3 a,8\3 g,8\4 d4\3 d8\3 g,4\4 |
    a,4\3 g,4\4 d4\3 g,8\4 r8 |
    g8\2 a4\2 g8\2 a4\2 e4\3 |
    a,4.\3 g,4\4 g,8\4 c8\3 c8\3 |
    a,8\3 a,16\3 a,16\3 a,8\3 g,8.\4 g,16\4 g,16\4 g,16\4 c8\3 b,8\3 |
    a,4.\3 g,8.\4 g,16\4 g,16\4 g,16\4 g,16\4 g,16\4 g,8\4 |
    c8\3 d8\3 e8\3 e8\3 d4\3 c8\3 b,8\3 |
    a,4.\3 g,4.\4 c4\3 |
    a,4.\3 g,4.\4 c4\3 |

    \time 3/4
    a,4.\3 g,4.\4 |

    \time 4/4
    c1\3 |
    a,4.\3 g,8\4~ g,2\4 |
    c4.\3 g,8\4~ g,2\4 |

    \time 3/4
    a,4.\3 g,4.\4 |

    \time 4/4
    a,1\3~ |
    a,2.\3 g,4\4 |
    a,2\3~ a,8\3 g'16\1 fis'16\1 \times 2/3 {e'16\1} \times 2/3 {fis'16\1} \times 2/3 {e'16\1}~ e'8\1 |
    e'2\1~ e'8\1 d'16\2 cis'16\2 b16\2 cis'16\2 b8\2 |
    a4\3 a,2.\3~ |
    a,2\3 r8 e'8\1 r8 e'8\1 |
    d'8\1 d'8\1 e'2.\1~ |
    e'2\1 r4 e'8\1 b8\2 |
    cis'1\2~ |
    cis'2\2~ cis'8\2 cis'16\2 cis'16\2 cis'8\2 cis'16\2 cis'16\2 |
    d'1\2~ |
    d'2\2~ d'8\2 cis'16\2 d'16\2 cis'8\2 b8\3 |
    a1\3~ |
    a1\3 |
    a,8\3 a,8\3 r4~ r2~ |
    r1 |
    fis1\2~ |
    fis1\2 |
    g1\2~ |
    g1\2 |
    d8\3 d8\3 r4~ r2~ |
    r1 |
    a,8\3 a,8\3 r4~ r4. a8\2~ |
    a1\2 |
    fis,8\4 fis,8\4 r4~ r4. fis8\2~ |
    fis1\2 |
    g8\2 g8\2 r4 b8.\3 g8.\3 g,8\4~ |
    g,1\4 |
    d8\3 d8\3 r4~ r4. d8\3~ |
    d1\3 |
    g4\2 e4\3 a4\2 a,4\3 |
    cis8\3 d4.\3 g4\2 fis4\2 |
    g4\2 e4\3 a4\2 a,4\3 |
    cis8\3 d4.\3 g8\2 g8\2 fis4\2 |

  }
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }
}

\book {
  \bookOutputSuffix "midi"
  \score {
    \unfoldRepeats {
      <<
        \new Staff { \transpose c c, {\Bass} }
      >>
    }
    \midi { }
  }
}
