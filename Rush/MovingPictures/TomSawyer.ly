\version "2.24.1"

%% Based on tabs from Sean Jones and Don Dianetti via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/mp-tom_sawyer.btab

\header {
  title = "Tom Sawyer"
  subtitle = "From 'Moving Pictures' by Rush (1981)"
  composer = "Geddy Lee"
}

Bass = \relative c, {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2

  \tempo 4 = 180
  \time 4/4
  \key e \minor

  %% e1~ | e1~ | e1~ | e1~ | e1~ | e1~ | e1~ | e1 |

  \repeat volta 2 {
    e8 e4 e8 e'2\3~ |
    e1\3 |
    e,8 e4 e8 d'2\3~ |
    d1\3 |
    e,8 e4 e8 a'2\2~ |
    a1\2 |
    e,8 e4 e8 c''2\2~ |
    c1\2 |
  }

  c4.\2 b\2 a4\2~ |
  a8\2 c4.\2 b4\2 a\2 |
  e,8 e4 e8 e'4.\3 e,8 |
  e8 e4 e8 fis'8 g\2 fis4 |
  d8 d4 d8 d'4. d,8~ |
  d8 d d d d' d, d'4 |
  a,8 a4 a8 a'4.\2 a,8~ |
  a8 a4 a8 a'\2 a, a'4\2 |
  c4.\2 b\2 a4\2~ |
  a8\2 g4.\3 fis4\3 e4\3 \bar "||"

  e,4. fis e8 fis |
  g4. a8 r a g a |
  e'4. c g8 c |
  b4. a g8 g |
  e4. fis8~ fis2 |
  g4. a8~ a2 |
  e'4. c8~ c2 |
  b4. a g8 g |

  e4. fis8~ fis2 |
  g4. a8~ a2 |
  e'4. c8~ c2 |
  b4. a g8 g |
  e4. fis e8 fis |
  g4. a8 r a g a |
  e'4. c g8 c |
  b8 fis b a r4 a |

  b1~ | b1~ |
  b1~ | b1 | a1\4 | g2. b4~ |
  b1 | a1\4 | g1 | a1\4 |
  e1~ | e1~ | e1~ | e1~ |
  e1~ | e1~ | e1~ | e1 \bar "||"

  \time 7/4
  e1~ e2.~ | e1~ e2.~ | e1~ e2.~ | e1~ e2. |

  \repeat volta 4 {
    e2~^\markup "4 times" e4. fis2~ fis4. |
  }
  e1~ e2.~ | e1~ e2. \bar "||"

  a'8\2 fis gis\2 e\3 d\3 [b\4 b\4] a'4\2 fis8 gis\2 e\3 d\3 b\4

  \repeat volta 2 {
    a'8\2 fis gis\2 e\3 d\3 [b\4 b\4] a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |
    a'8\2 fis16 fis gis8\2 e\3 d\3 [b\4 b\4] a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |
  }

  \time 13/8
  a'8\2 fis gis\2 e\3 d16\3 e\3 d8\3 b\4 a4\4 b\4 d\3 |
  \time 7/4
  a'8\2 fis gis\2 e\3 d\3 b4\4 a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |
  a'8\2 fis gis\2 e\3 d\3 [b\4 b\4] a'4\2 fis8 gis\2 e\3 d\3 b\4 |

  a'8\2 fis gis\2 e\3 d\3 [b\4 b\4] a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |
  \time 13/8
  a'8\2 fis16 fis gis8\2 e\3 d16\3 e\3 d8\3 b\4 a4\4 b\4 d\3 |
  \time 7/4
  a'8\2 fis gis\2 e\3 d\3 b4\4 a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |
  a'8\2 fis16 fis gis8\2 e\3 d\3 [b\4 b\4] a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |

  \repeat volta 2 {
    d'8 b cis a\2 g\2 e\3 e\3 d'4 b16 b cis8 a\2 g\2 e\3 |
  }
  a8\2 fis16 fis gis8\2 e\3 d\3 [b\4 b\4] a'4\2 fis16 fis gis8\2 e\3 d\3 b\4 |
  \time 7/8
  a'8\2 fis16 fis gis8\2 e\3 d16\3 e\3 d8\3 b |
  \time 4/4
  a4\4 b\4 d\3 e\3 \bar "||"

  e,8 e4 e8 e'2\3~ |
  e1\3 |
  e,8 e4 e8 d'2\3~ |
  d1\3 |
  e,8 e4 e8 a'2\2 |
  a,8. a a4 a8 a4 |
  e8 e e e c''2 |
  c,4. b a4 |

  e8 e4 e8 e'2\3~ |
  e1\3 |
  e,8 e4 e8 d'2\3~ |
  d1\3 |
  e,8 e4 e8 a'2\2~ |
  a1 |
  e,8 e4 e8 c''2~ |
  c8 c4 g8\2 c c c, c |
  c'4. b a4\2~ |
  a8 c8 c c b b a4\2 \bar "||"

  e,8 e4 e8 e'4.\3 e,8~ |
  e8 e4 e8 fis' g\2 fis g16\2 g\2 |
  d8 d4 d8 d'4 d8 d, |
  d8 d d' d, d' d, d'4 |
  a,8 a4 a8 a'4\2 a8\2 a,~ |
  a8 a a'\2 a, a a'\2 a\2 a\2 |
  c4. b a4\2~ |
  a8\2 g4.\2 g8\2 g\2 e4\3 \bar "||"

  e,4. fis e8 fis |
  g4. a g8 a |
  e'4. c g8 c |
  b4. a4 a8 g8 g |
  e4. fis8~ fis2 |
  g4. a8~ a2 |
  e'4. c8~ c2 |
  b4. a4. g8 g |

  e4. fis8~ fis2 |
  g4. a8~ a2 |
  e'4. c8~ c2 |
  b4. a8~ \times 2/3 {a4 g g} |
  e4. fis e8 fis |
  g4. a4 a8 g a |
  e'4. c g8 c |
  b8 fis b a~ \times 2/3 {a4 a a} \bar "||"

  b1~ | b1~ |
  b1~ | b1 | a1\4 | g2. b4~ |
  b1 | a1\4 | g1 | a1\4 |

  e1~ | e1~ | e1~ | e1~ |
  e1~ | e1~ | e1~ | e1~ |
  e1~ | e1~ |

  \time 7/4
  e1~ e2. |
  \time 13/8
  r2 r4. fis'4 cis fis |
  \time 7/4
  \repeat volta 3 {
    e2~^\markup "3 times" e4. fis4.~ fis2 |
  }
  \time 13/8
  e,2~ e4. fis4 cis' fis |
  \time 7/4
  e,2. e8 fis4. cis'4 fis |
  \repeat volta 3 {
    e2~^\markup "3 times" e4. fis4.~ fis2 |
  }
  \time 13/8
  e,2~ e4. fis4 cis' fis |


}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}

