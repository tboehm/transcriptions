\version "2.24.1"

%% Based on tabs from Sean Jones, Lionel Gibaudan, Don Dianetti, and Gianfranco Fiocco
%% via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/pow-grand_designs.btab

\header {
  title = "Grand Designs"
  subtitle = "From 'Power Windows' by Rush (1985)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2

  \key d \major

  R1*4 |

  \repeat volta 2 {
    d4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2~ |
    e1 |
  }

  d'4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2 |
  a''4 g a g \glissando \hideNotes \grace g,16 \unHideNotes |
  d4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2~ |
  e2. a'4\3 \glissando \hideNotes \grace d,16 \unHideNotes \bar "||"

  d8\3 d\3 cis d\3 \glissando e\3 e\3 d\3 e\3 |
  g,8 g fis g g16 g g g fis8 g |
  d'8\3 d\3 cis d\3 \glissando e\3 e\3 d\3 e16\3 e, |
  g8 g fis g r g16 g fis8 g |

  d'8\3 d16\3 d16\3 cis8 d\3 e\3 e\3 d\3 e16\3 e, |
  g8 g fis g g g16 g fis8 g
  \repeat volta 3 {
    d'8\3^\markup "3 times" d16\3 d16\3 cis8 d\3 e\3 e\3 d\3 e16\3 e, |
    g8 g fis g r g16 g fis8 g |
  }

  d'8\3 d16\3 d16\3 cis8 d\3 e\3 e16\3 e\3 d8\3 fis, |
  g16 g g g fis8 g g16 r g fis~ fis8 g \bar "||"

  d'8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b a |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b cis |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b a |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis b a cis |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis b a cis |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  a4\4 r2. \bar "||"

  d4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2~ |
  e1 |
  d'4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2~ |
  e2. a'4\3 \glissando \hideNotes \grace d,16 \unHideNotes \bar "||"

  e4\3 e,2 e'4\3~ |
  e4\3 e4.\3 b8\4 e\3 e\3 |
  d4\3 d'2 d,4\3~ |
  d4\3 d4.\3 a8\4 d\3 a'\2 |

  e4\3 e,2 e'4\3~ |
  e4\3 e4.\3 b8\4 e\3 e\3 |
  d4\3 d'2 d,4\3~ |
  d4\3 d4\3 d8\3 e\3 d\3 b\4 |

  a4\4 a'2\2 a,4\4~ |
  a4\4 a4.\4 a8\4 a\4 a\4 |
  b4\4 b'2\2 b,4\4~ |
  b4\4 b4\4 r b8\4 b\4 \bar "||"

  d8\3 d\3 cis8 d\3 e\3 e\3 d\3 e\3 |
  g,8 g fis g r g fis g |
  \repeat volta 3 {
    d'8\3^\markup "3 times" d\3 cis8 d\3 e\3 e\3 d\3 e16\3 e, |
    g8 g fis g r g fis g |
  }

  d'8\3 d\3 cis8 d\3 e\3 e\3 d\3 e16\3 e, |
  g8 g fis g g g fis g |
  d'8\3 d\3 cis8 d\3 e\3 e\3 d\3 e16\3 e, |
  g8 g fis g r g fis g |
  d'8\3 d\3 cis8 d\3 e\3 e\3 d\3 e16\3 e, |
  \times 2/3 {g4 g g} r8 fis g g'\2 \bar "||"

  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b a |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis b a cis |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b cis |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b a |
  d8\3 d\3 cis\3 d16\3 a g8 g fis g |
  b8 b a b cis cis b a |
  d8\3 d\3 cis\3 d\3 g, a\4 c d\4 \bar "||"

  e8\3 e\3 e,2 e'4\3 |
  e4\3 e4.\3 e,8 e'\3 e\3 |
  d4\3 d'2 d,4\3~ |
  d4\3 d4.\3 a8\4 d\3 a\4 |
  e'4\3 e,2 e'4\3~ |
  e4\3 e4.\3 a,8\4 d\3 d\3 |
  d4\3 d'2 d,4\3~ |
  d4\3 d8\3 a\4 d\3 e\3 d\3 b\4 |
  a4\4 a'2\2 a,4\4~ |
  a4\4 a2\4 a8\4 a\4 |
  b4\4 b'2\2 b,4\4~ |
  b4\4 b2\4 b8\4 b |

  d4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2 |
  r1 |
  d'4\3 d\3 \glissando \hideNotes \grace d'16 \unHideNotes e,,2 |
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}

