\version "2.24.1"

%% Based on tabs from Don Dianetti and Sean Jones via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/pow-marathon.btab

                                % TODO: Fix more-than-2x repeats

\header {
  title = "Marathon"
  subtitle = "From the 'Power Windows' by Rush (1985)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \key g \major

  %% %%%%%%%%%%%%%% %%
  %%     Intro      %%
  %% %%%%%%%%%%%%%% %%

  \repeat volta 5 {
    fis4 b, b'2~ |
    b1^\markup{"5 times"} |
  }

  e,,4 b'4 e2~ |
  e1 |

  %% %%%%%%%%%%%%%% %%
  %%     Verse 1b   %%
  %% %%%%%%%%%%%%%% %%

  \repeat volta 15 {
    fis8 [b,8~] b [b'~] b a16 a fis8 a^\markup{"15 times"} |
  }

  fis8 [b,8~] b [b'~] b a16 a fis8 b, |

  %% %%%%%%%%%%%%%% %%
  %%     Verse 1b   %%
  %% %%%%%%%%%%%%%% %%

  %% "More than just survival"
  \repeat volta 2 {
    b8 b b b b b b b |
  }

  %% "More than just a flash"
  d8\3 d\3 d\3 d\3 d\3 d\3 d\3 d\3 |
  d8\3 d\3 d\3 d\3 d\3 e\3 d\3 b\4 |

  %% "More than just a dotted line"
  g8 g g g g g fis g |

  %% "More than just a dash"
  a8\4 a\4 a\4 g a4\4 a\4 |

  \repeat volta 11 {
    fis'8 [b,8~] b [b'~] b a16 a fis8 a^\markup{"11 times"} |
  }

  fis8 [b,8~] b [b'~] b a16 a fis8 b, |

  %% "More than blind ambition"
  \repeat volta 2 {
    b8 b b b b b b b |
  }

  %% "More than simple greed"
  d8\3 d\3 d\3 d\3 d\3 d\3 d\3 d\3 |
  d8\3 d\3 d\3 d\3 d\3 e\3 d\3 b\4 |

  %% "More than just a finish line"
  g8 g g g g g fis g |

  %% "Must feed this burning need"
  a4\4 a8\4 g a4\4 g8 a\4 |
  b4 b8 b b fis b4~ |

  %% "In the long run..."
  b8 b b b b b b fis |
  g8 g g g g g fis g |
  a2.\4 \glissando b'4\2 \glissando |

  %% %%%%%%%%%%%%%% %%
  %%     Chorus 1   %%
  %% %%%%%%%%%%%%%% %%

  %% "From first to last"
  b,1~ |
  b2 e,2 |

  %% "The peak is never passed"
  fis1~ |
  fis1 |

  %% "Something always fires that ---"
  e1~ |
  e1 |

  %% "--- light that gets in your ---
  gis1 |

  %% "--- eyes"
  ais1\4 |

  %% "One moment's high"
  b1~ |
  b2 e |

  %% "and glory rolls on by"
  fis,1~ |
  fis2 fis2 |

  %% "Like a streak of lightning"
  e1~ |
  e1 |

  %% "That flashes and fades in the summer sky"
  e8 e e e e e e e |
  fis8 fis fis fis fis fis fis fis |

  %% %%%%%%%%%%%%%% %%
  %%      Bridge    %%
  %% %%%%%%%%%%%%%% %%

  \repeat volta 2 {
    fis'4 b, b'4 b, |
    b1 |
  }

  e,4 b' e e, |
  e'8 e~ e2. |

  %% %%%%%%%%%%%%%% %%
  %%     Verse 2a   %%
  %% %%%%%%%%%%%%%% %%

  \repeat volta 4 {
    fis8 [b,8~] b b' r8 b a fis |
    b,4 b'16 [b fis8] a16 [a fis8] b4^\markup{"4 times"} |
  }

  fis8 [b,8~] b b' r8 b a fis |
  b,4 b'16 [b fis8] a8 [fis] e [d] |

  %% "More than high performance"
  \repeat volta 2 {
    b8 b b b b b b b |
  }

  %% "More than just a spark"
  d8\3 d\3 d\3 d\3 d\3 d\3 d\3 d\3 |
  d8\3 d\3 d\3 d\3 d\3 e\3 d\3 b\4 |

  %% "More than just a bottom line"
  g8 g g g g g fis g |

  %% "Or a lucky shot in the dark"
  a8\4 a\4 a\4 g a\4 a\4 g a\4 |
  b8 b b b8~ b4 b4~ |
  b8 b b b b b b fis |
  g8 g g g g g fis g |
  a2\4 \times 2/3 { a4\4 a\4 a\4 } |

  %% %%%%%%%%%%%%%% %%
  %%     Chorus 2   %%
  %% %%%%%%%%%%%%%% %%

  %% "From first to last"
  b1~ |
  b2 e,2 |

  %% "The peak is never passed"
  fis1~ |
  fis1 |

  %% "Something always fires that ---"
  e1~ |
  e1 |

  %% "--- light that gets in your ---
  gis1 |

  %% "--- eyes"
  ais1\4 |

  %% "One moment's high"
  b1~ |
  b2 e |

  %% "and glory rolls on by"
  fis,1~ |
  fis2 fis2 |

  %% "Like a streak of lightning"
  e1~ |
  e1 |

  %% "That flashes and fades in the summer sky"
  e8 e e e e e e e |
  fis8 fis fis fis fis fis fis fis |

  %% %%%%%%%%%%%%%% %%
  %%      Solos!    %%
  %% %%%%%%%%%%%%%% %%

  \time 7/4

  \repeat volta 2 {
    d'8 d d e a, fis' b, d~ d d e a, fis' b, |
    d8 a d e a, fis' b, d a' d, e a, fis' b, |
  }


  d8 d d e a, fis' b, d~ d d e a, fis' b, |
  a8 a a b e, b' e, a~ a a b e, cis' e, |
  d'8 d d e a, fis' b, d~ d d e a, fis' b, |
  a8 fis a b8. r16 b8 e, a~ a b b e, b' e, |

  d'8 d d e a, fis' b, d~ d d e a, fis' b, |
  d8 d d e a, fis' a, d~ d d e e fis fis |

  \repeat volta 4 {
    b,1~ b2.~ |
    b1~ b2. |
    e,1~ e2.~ |
    e1~ e2.^\markup{"4 times"} |
  }

  b'1~ b2.~ |
  b1~ b2. |
  e,1~ e2.~ |
  e8 e e e e e e e e e e e e e |

  %% "You can do a lot in a lifetime"
  b'1~ b2.~ |
  b1~ b2. |
  e,1~ e2.~ |
  e1~ e2. |

  %% "You can make the most of the distance"
  b'1~ b2.~ |
  b1~ b2. |
  e1~ e2 e8 e |
  e8 e e e e e e e e e e e e e |

  d8 d d e a, fis' b, d a' d, e a, fis' b, |
  d8 d d e a, fis' b, d~ d d e a, fis' b, |
  d8 d d e a, fis' b, d a' d, e a, fis' b, |
  d8 d d e a, fis' b, a a' a, a' a, a' a, |

  %% %%%%%%%%%%%%%% %%
  %%     Chorus 3   %%
  %% %%%%%%%%%%%%%% %%

  \time 4/4
  %% "From first to last"
  b1~ |
  b2 e,2 |

  %% "The peak is never passed"
  fis1~ |
  fis1 |

  %% "Something always fires that ---"
  e1~ |
  e1 |

  %% "--- light that gets in your ---
  gis1 |

  %% "--- eyes"
  ais1\4 |

  %% "One moment's high"
  b1~ |
  b2 e |

  %% "and glory rolls on by"
  fis,1~ |
  fis2 fis2 |

  %% "Like a streak of lightning"
  e1~ |
  e1 |

  %% "That flashes and fades in the summer sky"
  e1 |
  fis1 |

  %% KEY CHANGE!
  \key a \major
  cis'1~ |
  cis2 fis, |

  gis1~ |
  gis1 |

  fis1~ |
  fis1 |

  ais1 |
  bis1 |

  cis1~ |
  cis2 fis, |

  gis1~ |
  gis2 gis |

  fis1~ |
  fis1 |

  fis1 |
  gis1 |

  %% %%%%%%%%%%%%%% %%
  %%      Outro     %%
  %% %%%%%%%%%%%%%% %%

  \key g \major
  b1~ |
  b2 e,2 |

  fis1~ |
  fis1 |

  e1~ |
  e1 |

  gis1 |
  ais1\4 |

  b1~ |
  b2 e |

  fis,1~ |
  fis2 fis2 |

  e1~ |
  e1 |

  e1 |
  fis1 |

  %% A bit more
  b1~ |
  b2 e, |
  fis1 |
}


\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}

