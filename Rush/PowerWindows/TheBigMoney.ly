\version "2.24.1"

%% Based on tabs from Erik Habbinga and Sean Jones via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/pow-the_big_money.btab

\header {
  title = "The Big Money"
  subtitle = "From 'Power Windows' by Rush (1985)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  %%\override Score.BarNumber.break-visibility = #'#(#f #f #t)
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \key g \major
  \tempo 4 = 140

  \absolute {

    %% FIXME: add intro

    \time 6/4
    \repeat volta 8 {
      e8\3^\markup "8 times" e8\3 b,8\4 e8\3 d8\3 d8\3 d8\3 e8\3 a,8\4 a,8\4 fis8\2 d8\3 |
    }

    \time 4/4
    e8\3 e8\3 e'4\1 d8\3 d8\3 d'4\1 |

    e8\3 e'8\1 r8 d'8\1 a8\2 e'8\1 d8\3 e8\3 |
    e'8\1 d8\2 d'8\1 d8\2 g16\2 gis16\2 e8\3 b,8\4 d8\3 |
    e8\3 e'8\1 r8 d'8\1 a8\2 e'8\1 d8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 d'16\1 d'16\1 b8\2 b,8\4 d8\3 |

    e8\3 e'8\1 r8 d'8\1 a8\2 e'8\1 d8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 g16\2 gis16\2 e8\3 b,8\4 d8\3 |
    e8\3 e'8\1 r8 d'8\1 a8\2 e'8\1 d8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 d'16\1 e'16\1 d'16\1 b16\2 a8\2 b8\2 |

    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 d8\3 b,8\4 fis,8\5 a,8\4 |
    b,8\4 b8\2 r8 a8\2 fis8\3 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 a16\2 a16\2 fis8\3 e8\3 fis8\3 |

    b,8\4 b8\2 fis8\3 a8\2 r8 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 d16\3 d16\3 b,8\4 fis,8\5 a,8\4 |
    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 e16\3 e16\3 e8\3 r4 |

    \time 6/4
    \repeat volta 5 {
      e8\3^\markup "5 times" e8\3 b,8\4 e8\3 d4.\3 e8\3 a,8\4 a,8\4 fis8\2 d8\3 |
    }
    e8\3 e8\3 b,8\4 e8\3 d4.\3 e8\3 a,8\4 fis8\2 g8\2 gis8\2 |
    \repeat volta 3 {
      a8\2^\markup "3 times" a8\2 e8\3 a8\2 g4.\2 a8\2 d8\3 d8\3 b8\1 g8\2 |
    }
    a8\2 a8\2 e8\3 a8\2 g4.\2 a8\2 b,8\4 cis8\3 d8\3 dis8\3 |
    e8\3 e8\3 e'4\1 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |

    \time 4/4
    e8\3 e'8\1 r8 d'8\1 a8\2 e'8\1 e8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 gis8\2 e8\3 b,8\4 d8\3 |
    e8\3 e'8\1 r8 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 b8\2 d'16\1 d'16\1 b8\2 d'16\1 d'16\1 b8\2 b,8\4 d8\3 |

    e8\3 e'8\1 r8 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 g16\2 gis16\2 e8\3 b,8\4 d8\3 |
    e8\3 e'8\1 r8 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 d'8\1 cis'8\1 d'8\1 cis'16\1 d'16\1 cis'16\1 b16\2 a16\2 b8.\2 |

    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 d8\3 b,8\4 fis,8\5 a,8\4 |
    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 gis8\2 a8\2 gis8\2 fis8\3 |

    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 d8\3 b,8\4 fis,8\5 a,8\4 |
    b,8\4 b8\2 fis8\3 a8\2 fis8\3 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 e8\3 e8\3 b8\2 fis8\3 |

    \time 6/4
    \repeat volta 5 {
      e8\3^\markup "5 times" e8\3 b,8\4 e8\3 d4.\3 e8\3 a,8\4 a,8\4 fis8\2 d8\3 |
    }
    e8\3 e8\3 b,8\4 e8\3 d4.\3 e8\3 a,8\4 fis8\2 g8\2 gis8\2 |
    \repeat volta 3 {
      a8\2^\markup "3 times" a8\2 e8\3 a8\2 g4.\2 a8\2 d8\3 d8\3 b8\1 g8\2 |
    }
    a8\2 a8\2 e8\3 a8\2 g4.\2 a8\2 b,8\4 cis8\3 d8\3 dis8\3 |
    e8\3 e8\3 e'4\1 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |
    \repeat volta 3 {
      e8\3^\markup "3 times" e8\3 e,4\4 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |
    }

    %% FIXME: these should be cue notes for bass pedals
    %% FIXME: add repeats
    \time 4/4
    e4.\3 d8\3~ d2\3~ |
    d1\3 |
    e,2\4 fis,2\4~ |
    fis,1\4 |
    e,2\4 d2\3~ |
    d1\3 |
    e,2\4 fis,2\4~ |
    fis,2\4~ fis,4.\4 gis16\2 gis16\2 |

    \time 2/4
    a8\2 gis16\2 gis16\2 a8\2 d8\3~ |

    \time 4/4
    d8\3 d8\3 r8 d8\3 cis16\3 cis16\3 d8\3 r8 d8\3 |
    cis16\3 cis16\3 d8\3 cis8\3 d8\3 cis8\3 b,8\4 a,8\4 e,8\4 |
    fis,8\4 fis,8\4 e,8\4 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    fis,8\4 g16\2 gis16\2 a8\2 b16\1 cis'16\1 d'8\1 a8\2 e8\3 d8\3~ |
    d8\3 d8\3 r8 d8\3 cis16\3 cis16\3 d8\3 cis8\3 d8\3 |
    e8\3 fis8\3 e8\3 d8\3 cis8\3 b,8\4 a,8\4 e,8\4 |
    fis,8\4 fis,8\4 e,8\4 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    fis,8\4 g'16\1 gis'16\1 a'8\1 a'16\1 g'16\1 e'8\2 b8\3 fis'8\1 d'8\2~ |
    d'8\2 d'8\1 a8\2 d4\3 d8\3 a,8\4 d8\3~ |
    d8\3 e8\3 d8\3 cis8\3 r8 b,8\4 r8 e,8\4 |
    fis,8\4 fis,8\4 e,8\4 fis,8\4 fis,8\4 fis,8\4 e,8\4 fis,8\4 |
    fis,8\4 g16\2 gis16\2 a16\2 d16\2 g16\2 d16\2 fis16\2 d16\2 g16\2 d16\2 fis8\2 d8\3~ |
    d8\3 d8\3 r8 d4\3 d8\3 r8 d8\3~ |
    d8\3 d8\3 cis8\3 d8\3 cis8\3 b,8\3 a,8\3 fis8\3~ |
    fis8\3 fis8\3 b,16\4 b,16\4 e4\3 e8\3 b,16\4 b,16\4 dis8\3~ |
    dis8\3 dis8\3 dis8\3 e4\3 e8\3 b,8\4 e8\3 |
    fis8\3 cis8\4 fis8\3 e4\3 b,8\4 e8\3 dis16\3 dis16\3 |
    dis8\3 e8\3 fis8\3 a8\2 b8\2 cis'8\1 d'8\1 e'8\1 |

    \time 6/4
    a8\2 a8\2 e8\3 a8\2 g8\2 g8\2 a8\2 a8\2 d8\3 d8\3 b4\1 |
    a8\2 a8\2 e8\3 a8\2 g8\2 g8\2 a8\2 a8\2 d8\3 d8\3 b4\1 |
    a8\2 a8\2 e8\3 a8\2 g8\2 g8\2 a8\2 a8\2 d8\3 d8\3 b4\1 |
    a8\2 a8\2 e8\3 a8\2 g8\2 g8\2 a8\2 a8\2 r8 cis8\3 d8\3 dis8\3 |
    e8\3 e8\3 e'4\1 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |

    \time 4/4
    e8\3 e'8\1 r8 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 g16\2 gis16\2 e8\3 b,8\4 d8\3 |
    e8\3 e'8\1 b8\2 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 d'16\1 d'16\1 b8\2 b,8\4 d8\3 |

    e8\3 e'8\1 r8 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 b8\2 d'8\1 b8\2 g8\2 e8\3 b,8\4 d8\3 |
    e8\3 e'8\1 r8 d'8\1 a8\2 d'8\1 e8\3 e8\3 |
    e'8\1 d'8\1 cis'8\1 b8\2 ais8\2 a8\2 g16\2 e8.\3 |

    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 d16\3 d16\3 b,8\4 fis,8\5 a,8\4 |
    b,8\4 b8\2 fis8\3 a4\2 b8\2 b,8\4 b,8\4 |
    b8\2 fis8\3 a8\2 fis8\3 gis8\2 a8\2 gis8\2 e8\3 |

    %% FIXME: there are missing measures of eighth notes
    b,8\3 b,8\3 b,8\3 b,8\3 b,8\3 b,8\3 b,8\3 b,8\3 |
    a,2\3 a,2\3 |
    %% FIXME: add repeats
    b,4.\3 b,4.\3 b,4\3 |
    b,4.\3 b,8\3~ b,2\3 |

    \times 2/3 {b,4\3 b,4\3 b,4\3~} b,2\3~ |
    b,1\3~ |
    b,1\3 |

    \time 6/4
    r1~ r2 |
    r1 e'16\1 e'16\1 e'16\1 e'16\1 e'16\1 e'16\1 e'16\1 e'16\1 |

    %% FIXME: add repeats
    e'8\1 e'8\1 e,4\4 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |
    e8\3 e8\3 e,4\4 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |
    e8\3 e8\3 e,4\4 d8\3 d8\3 d'4\1 a,8\4 a,8\4 fis8\2 d8\3 |
    e8\3 e8\3 e,4\4 d8\3 d8\3 d'8\1 d'8\1 a,8\4 a,8\4 fis8\2 d8\3 |
  }
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-five-string-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }
}

\book {
  \bookOutputSuffix "midi"
  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}
