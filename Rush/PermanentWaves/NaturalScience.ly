\version "2.24.1"

%% Based on tabs by Eric Habbinga, Sean Jones, and Don Dianetti via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/pw-natural_science.btab

\header {
  title = "Natural Science"
  subtitle = "From 'Permanent Waves' by Rush (1980)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \key g \major

  \absolute {

    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4\3 b,8\3 c8\3 c8\3 c8\3 d8\2 |
    e8\2 e8\2 e8\2 c4\3 c8\3 d8\2 d8\2 |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e8\2 e8\2 e8\2 c4\3 c8\3 d8\2 d8\2 |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e8\2 e8\2 e8\2 c4\3 c8\3 d4\2 |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e4.\2 c4.\3 d4\2~ |

    \time 7/4
    d1\2~ d2.\2 |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |

    \time 2/4
    c'8\1 b8\1 a8\2 g8\2 |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |
    c'8\1 b8\1 a8\2 g8\2 fis8\2 e8\3 d8\3 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 b,8\4 a,8\4 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 d8\3 e8\3 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 b,8\4 a,8\4 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 d8\3 e8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    g8\2 fis8\2 e8\3 d8\3 cis8\3 a,8\3 e8\2 |

    \time 4/4
    b,4.\3 b,4\3 b,8\3 fis,8\4 b,8\3 |
    a,4.\3 a,4\3 a,8\3 r8 a,8\3 |
    g8\2 d8\3 g8\2 d8\3 g8\2 d8\3 g8\2 a8\2 |
    fis8\2 g8\2 a8\2 g8\2 fis8\2 e8\3 d8\3 cis8\3 |
    b,4\3 r8 b,8\3 r8 b,8\3 fis,8\4 b,8\3 |
    a,4\3 r8 a,8\3 a,8\3 a,8\3 a,8\3 a,8\3 |
    g8\2 g8\2 d8\3 g8\2 g8\2 d8\3 g8\2 d8\3 |
    \times 2/3 {fis4\2} \times 2/3 {fis4\2} \times 2/3 {fis4\2} g8\2 fis8\2 e8\3 d8\3 |
    cis4\3 cis8\3 cis8\3 gis,8\4 gis,8\4 cis8\3 c8\3 |
    b,4\3 b,4\3 fis,8\4 fis,8\4 b,4\3 |
    a8\2 a8\2 e8\3 a8\2 a8\2 e8\3 a8\2 b8\2 |
    gis8\2 gis8\2 gis8\2 e8\3 gis8\2 a8\2 gis8\2 fis8\3 |
    cis4\3 cis4\3 gis,8\4 gis,8\4 cis8\3 c8\3 |
    b,4\3 b,4\3 fis,4\4 b,4\3 |
    a8\2 a8\2 e8\3 a8\2 a8\2 e8\3 gis8\2 gis8\2 |
    e8\3 gis8\2 gis8\2 e8\3 gis8\2 cis'8\1 b4\2 |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |

    \time 2/4
    c'8\1 b8\1 a8\2 g8\2 |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |
    b8\1 a8\2 b8\1 a8\2 b8\1 a8\2 g8\2 |
    b8\1 a8\2 b8\1 a8\2 b8\1 c'8\1 d'8\1 |
    c'8\1 b8\1 a8\2 g8\2 fis8\2 e8\3 d8\3 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 b,8\4 a,8\4 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 d8\3 e8\3 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 b,8\4 a,8\4 |
    cis8\3 b,8\4 cis8\3 b,8\4 cis8\3 d8\3 e8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 e8\3 d8\3 |
    fis8\2 e8\3 fis8\2 e8\3 fis8\2 g8\2 a8\2 |
    g8\2 fis8\2 e8\3 d8\3 cis8\3 a,8\3 e8\2 |

    \time 4/4
    b,4\3 r2. |
    r1 |
    b,4.\3 b,8\3 r8 b,8\3 fis,8\4 b,8\3 |
    a,4.\3 a,4\3 a,8\3 a,8\3 a,8\3 |
    g8\2 d8\3 g8\2 d8\3 g8\2 d8\3 g8\2 d8\3 |
    fis8\2 g8\2 a8\2 g8\2 fis16\2 fis16\2 d8\3 cis8\3 c8\3 |
    b,4.\3 b,8\3 r8 b,8\3 fis,8\4 b,8\3 |
    a,8\3 a,8\3 r8 a,8\3 a,8\3 a,8\3 a,8\3 a,8\3 |
    g8\2 g8\2 a8\2 a8\2 b8\1 b8\1 fis8\2 fis8\2 |
    g8\2 g8\2 cis8\3 cis8\3 d8\3 fis8\2 e8\3 cis8\3 |
    b,4.\3 b,8\3 r8 b,8\3 fis,16\4 fis,16\4 b,8\3 |
    a,8\3 a,8\3 g,8\4 a,8\3 a,8\3 g,8\4 a,8\3 g,8\4 |
    g8\2 g8\2 d8\3 g8\2 g8\2 d8\3 g16\2 g16\2 d8\3 |
    g16\2 g16\2 d8\3 g16\2 g16\2 d8\3 g16\2 g16\2 d8\3 g8\2 cis8\3 |
    b,4.\3 b,4.\3 fis,8\4 b,8\3 |
    a,8\3 a,8\3 g,8\4 a,8\3 a,8\3 g,8\4 a,8\3 g,8\4 |
    a4\2 e8\3 g4\2 g8\2 fis8\2 fis8\2 |
    d8\3 fis8\2 g8\2 fis8\2 g8\2 fis8\2 g8\2 d8\3 |
    cis4.\3 cis4\3 cis8\3 gis,8\4 cis8\3 |
    b,8\3 b,8\3 r8 b,4\3 b,8\3 b,8\3 d8\3 |
    a8\2 a8\2 b8\2 b8\2 a8\2 a8\2 gis8\2 gis8\2 |
    e8\3 e8\3 gis4\2 e8\3 e8\3 gis8\2 e8\3 |
    cis4.\3 cis4\3 cis8\3 gis,8\4 cis8\3 |
    b,8\3 b,8\3 r8 b,8\3 b,8\3 b,8\3 cis8\3 d8\3 |
    a8\2 a8\2 e8\3 a8\2 a8\2 e8\3 gis8\2 gis8\2 |
    e8\3 gis8\2 gis8\2 e8\3 gis8\2 e8\3 gis8\2 e8\3 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4\3 b,8\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c4\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 d4\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e8\2 e8\2 e8\2 c4.\3 d4\2~ |
    d8\2 b,4.\3 c8\3 b,8\3 c8\3 d8\2 |
    e4.\2 c4.\3 d4\2~ |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e8\2 e8\2 e8\2 c4\3 c8\3 d8\2 d8\2 |
    d8\2 b,4\3 b,8\3 c8\3 b,8\3 c8\3 d8\2 |
    e4.\2 c4.\3 d4\2~ |

    \time 2/4
    d8\2 b,4.\3 |

    \time 3/4
    c2.\3 |
    d2.\2 |

    \time 4/4
    b,8\3 b,8\3 b,8.\3 fis,16\4 b,8\3 fis,8\4 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    b,4\3 b,8.\3 fis,16\4 b,8\3 a,8\3 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    r8 a4\2 e8\3 e8\3 e,8\4 a,8\3 ais,8\3 |
    b,4\3 b,8.\3 fis,16\4 b,8\3 fis,8\4 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    r8 a4\2 e8\3 e8\3 e,8\4 b,16\4 b,16\4 e8\3 |


    %% \times 2/3 {d2\3 d4\3 a2\2 a4\2} |
    %% \times 2/3 {g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2} |
    %% \times 2/3 {e4.\3 e4\3 b,8\4 e4\3 b,4\4 e4\3} |
    %% d8\3 r8 e8\3 r8 a8\2 r8 e8\3 r8 |
    %% \times 2/3 {fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3} |
    %% \times 2/3 {b,4\4 cis4\3 d4\3 g4\2 fis4\2 e4\3} |
    %% e,8.\4 e,16\4 e,4\4 \times 2/3 {e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3} |
    %% d4\3 d4\3 \times 2/3 {a4\2 d4\3 a4\2} |
    %% \times 2/3 {g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2} |
    %% e4\3 \times 2/3 {e4\3 b,8\4 e4\3 b,4\4 e4\3} |
    %% d4\3 e4\3 a4\2 e4\3 |
    %% \times 2/3 {fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3} |
    %% \times 2/3 {b,4\4 cis4\3 cis8\3 d8\3 g4\2 fis4\2 e8\2 fis8\2} |
    %% e,8.\4 e,16\4 e,4\4 \times 2/3 {e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3} |
    %% b,4\3 b,8.\3 fis,16\4 b,8\3 fis,8\4 b,8\3 fis,8\4 |
    %% a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    %% e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    %% b,4\3 b,8.\3 b,16\3 b,8\3 a,8\3 b,8\3 ais,8\3 |
    %% a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    %% e4\3 e8.\3 b,16\4 e8\3 e8\3 b,8\4 e8\3 |
    %% \times 2/3 {d4\3 d4\3 d4\3 a4\2 d4\3 a4\2} |
    %% \times 2/3 {g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2} |
    %% \times 2/3 {e8\3 e4\3 e4\3 b,8\4 e4\3 b,4\4 e4\3} |
    %% d4\3 e4\3 a4\2 e4\3 |
    %% \times 2/3 {fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3} |
    %% \times 2/3 {b,4\4 cis4\3 cis8\3 d8\3 g8\2 fis8\2 r8 e8\2 fis8\2 r8} |
    %% e,8.\4 e,16\4 e,4\4 \times 2/3 {e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3} |
    %% \times 2/3 {d4.\3 d4\3 d8\3 a4\2 d4\3 a4\2} |
    %% \times 2/3 {g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2} |
    %% \times 2/3 {e8\3 e4\3 e4\3 b,8\4 e4\3 b,4\4 e4\3} |
    %% d8\3 r8 e8\3 r8 a8\2 r8 e8\3 r8 |
    %% fis4\2 \times 2/3 {d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3} |
    %% \times 2/3 {b,4\4 cis4\3 d4\3 g4\2 fis4\2 e4\3} |
    %% \times 2/3 {e,4\4 e,8\4 e,4.\4 e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3} |

    \time 12/8
    d2\3 d4\3 a2\2 a4\2 |
    g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2 |
    e4.\3 e4\3 b,8\4 e4\3 b,4\4 e4\3 |
    d8\3 r4 e8\3 r4 a8\2 r4 e8\3 r4 |
    fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3 |
    b,4\4 cis4\3 d4\3 g4\2 fis4\2 e4\3 |
    \times 3/2 {e,8.\4 e,16\4} e,4.\4 e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3 |
    d4.\3 d4.\3 a4\2 d4\3 a4\2 |
    g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2 |
    e4.\3 e4\3 b,8\4 e4\3 b,4\4 e4\3 |
    d4.\3 e4.\3 a4.\2 e4.\3 |
    fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3 |
    b,4\4 cis4\3 cis8\3 d8\3 g4\2 fis4\2 e8\2 fis8\2 |
    \times 3/2 {e,8.\4 e,16\4} e,4.\4 e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3 |

    \time 4/4
    b,4\3 b,8.\3 fis,16\4 b,8\3 fis,8\4 b,8\3 fis,8\4 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    b,4\3 b,8.\3 b,16\3 b,8\3 a,8\3 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 e8\3 b,8\4 e8\3 |

    \time 12/8
    d4\3 d4\3 d4\3 a4\2 d4\3 a4\2 |
    g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2 |
    e8\3 e4\3 e4\3 b,8\4 e4\3 b,4\4 e4\3 |
    d4.\3 e4.\3 a4.\2 e4.\3 |
    fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3 |
    b,4\4 cis4\3 cis8\3 d8\3 g8\2 fis8\2 r8 e8\2 fis8\2 r8 |
    \times 3/2 {e,8.\4 e,16\4} e,4.\4 e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3 |
    d4.\3 d4\3 d8\3 a4\2 d4\3 a4\2 |
    g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2 |
    e8\3 e4\3 e4\3 b,8\4 e4\3 b,4\4 e4\3 |
    d8\3 r4 e8\3 r4 a8\2 r4 e8\3 r4 |
    fis4.\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3 |
    b,4\4 cis4\3 d4\3 g4\2 fis4\2 e4\3 |
    e,4\4 e,8\4 e,4.\4 e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3 |

    \time 4/4
    b,4\3 b,4\3 b,8\3 fis,8\4 b,8\3 fis,8\4 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    b,4\3 b,8.\3 fis,16\4 b,8\3 a,8\3 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    b,4\3 b,8.\3 fis,16\4 b,8\3 fis,8\4 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    b,4\3 b,8.\3 fis,16\4 b,8\3 a,8\3 b,8\3 ais,8\3 |
    a,4\3 a,8.\3 e,16\4 a,8\3 cis8\3 d8\3 dis8\3 |
    e4\3 e8.\3 b,16\4 e8\3 dis8\3 e8\3 dis8\3 |
    r8 a4\2 e8\3 e8\3 e8\3 b,8\4 e8\3 |

    \time 12/8
    d4.\3 d4\3 d8\3 a4\2 d4\3 a4\2 |
    g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2 |
    e4.\2 e4\2 b,8\3 e8\2 e8\2 b,4\3 e4\2 |
    d8\3 r4 e8\3 r4 a8\2 r4 e4. |
    fis4.\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3 |
    b,4\4 cis4\3 d4\3 g4\2 fis4\2 e8\3 fis8\2 |
    \times 3/2 {e,8.\4 e,16\4} e,4.\4 e,8\4 fis,8\4 gis,8\4 a,8\3 b,8\3 cis8\3 |
    d4.\3 d4\3 d8\3 a4\2 d4\3 a4\2 |
    g8\2 g4\2 g4\2 d8\3 fis4\2 g4\2 fis4\2 |
    e4.\2 e4\2 b,8\3 e8\2 e8\2 b,8\3 b,8\3 e4\2 |
    d8\3 r4 e8\3 r4 a4.\2 e4.\3 |
    fis4\2 fis8\2 d8\3 fis8\2 d8\3 e4\3 d4\3 cis4\3 |
    b,4\4 cis4\3 d4\3 g4\2 fis4\2 e4\3 |

    \repeat volta 6 {
      e2.\2^\markup "6 times" c2.\3 |
      d2.\3 b,4\3 c4\3 d4\3 |
    }

    b,4\3 b,8\3 fis,8\4 b,8\3 ais,8\3 a,4\3 a,8\3 e,8\4 a,8\3 e,8\4 |
    g8\2 g8\2 g8\2 d8\3 g8\2 d8\3 fis8\2 d8\3 fis8\2 g8\2 fis8\2 d8\3 |
    b,4\3 b,8\3 fis,8\4 b,8\3 ais,8\3 a,4\3 a,4.\3 a,8\3 |
    a8\2 a8\2 a8\2 g8\2 g8\2 g8\2 fis8\2 g8\2 fis8\2 g8\2 fis8\2 d8\3 |
    cis4\3 cis8\3 gis,8\4 cis8\3 c8\3 b,4\3 b,8\3 fis,8\4 b,8\3 cis8\3 |
    e8\2 e8\2 e8\2 b,8\3 e8\2 b,8\3 e8\2 f8\2 fis8\2 fis8\2 e8\2 d8\3 |
    cis4\3 cis8\3 gis,8\4 cis8\3 c8\3 b,4\3 b,8\3 fis,8\4 b,8\3 cis8\3 |
    e4\2 e8\2 b,8\3 e8\2 f8\2 fis4\2 fis16\2 fis16\2 fis8\2 e8\2 d8\3 |
    b,4\3 b,8\3 fis,8\4 b,8\3 fis,8\4 a,4\3 a,8\3 e,8\4 a,8\3 e,8\4 |
    g8\2 g8\2 g8\2 d8\3 g8\2 d8\3 fis8\2 d8\3 fis8\2 a4.\2 |
    b,4\3 b,8\3 fis,8\4 b,8\3 fis,8\4 a,4\3 a,8\3 a,8\3 a,8\3 a,8\3 |
    a4.\2 fis4\2 fis8\2 g8\2 fis8\2 g8\2 d8\3 g8\2 fis8\2 |

    cis4\3 cis8\3 gis,8\4 cis8\3 c8\3 b,4\3 b,8\3 fis,8\4 b,8\3 cis8\3 |
    e4\2 e8\2 b,8\3 e8\2 b,8\3 e8\2 fis8\2 fis8\2 e8\2 fis8\2 fis8\2 |
    cis4\3 cis8\3 gis,8\4 cis8\3 c8\3 b,4\3 b,2\3 |
    e4\2 e8\2 b,8\3 e8\2 b,8\3 fis4\2 fis8\2 fis8\2 e8\2 d8\3 |
    b,4\3 b,4\3 b,4\3 cis2\3 cis8\3 cis8\3 |
    cis2\3 cis8\3 cis8\3 cis8\3 gis,8\4 cis8\3 gis,8\4 cis8\3 c8\3 |
    b,4\3 cis4\3 b,4\3 a,4.\4 a8\2 a,8\4 a,8\4 |
    e4.\3 a8\2 a,8\4 a8\2 e4\3 e8\3 e4\3 e8\3 |
    b,4\3 fis,4\4 b,4\3 cis2\3 cis8\3 cis8\3 |
    cis2\3 cis8\3 cis8\3 cis8\3 gis,8\4 cis8\3 gis,8\4 cis8\3 c8\3 |
    b,4\3 cis4\3 b,4\3 a,2.\3 |
    e2.\2 a2.\1 |
    e4\2 a4\1 b,4\3 cis2.\3~ |
    cis1.\3~ |
    cis1.\3~ |
    cis1.\3 |
  }
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}
