\version "2.24.1"

%% Based on tabs by  Mark Mitchell and Sean Jones via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/pw-freewill.btab

\header {
  title = "Freewill"
  subtitle = "From 'Permanent Waves' by Rush (1980)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2

  \numericTimeSignature
  \tempo 4 = 160
  \time 4/4 f4 r4. f4. | c4 r4. c4. |
  \time 6/4 f,1~ f2~ |
  \time 7/4  f1~ f2.
  \bar "||"
  \time 6/4 f4 f8 c'4 c8 b4 a g |
  \time 7/4 f4 f8 c'4 c8 e4 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 7/4 f4 f8 c'4 c8 e4 d b8 c g'4\2
  \bar "||"
  \time 6/4 f,4^\markup "There are those who think that" f8 c'4. b4 a g |
  \time 4/4 f4^\markup "Life has nothing" f8 c'4 c8 e4 | d4^\markup "left to chance a" c d e |
  \time 6/4 f,4^\markup "host of holy horrors" f8 c'4. b4 a g |
  \time 7/4 f4^\markup "to direct our aimless dance." f8 c'4 c8 e4 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 7/4 f4 f8 c'4 c8 e4 d8 d16 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 4/4 f4 f8 c'4 c8 e4 | d4 c d e^\markup "A"
  \bar "||"

  b4^\markup "planet of" b4. b4 fis8 |
  bes4^\markup "play things we" bes4. bes4 g8 |
  f'4^\markup "dance on the" f4. f4 b,8 |
  c4^\markup "strings of" c c8 c4 g8 |
  b4^\markup "powers we" b4. b4 fis8 |
  bes4^\markup "cannot perceive." bes4. bes4 g8 |
  d'4.\3 a'\2 d4 |
  r8 a\2 r a\2 d,\3 a'\2 d,\3 a |

  b4 b4.^\markup "'The stars aren't aligned" b4 fis8 |
  bes4 bes4. bes4^\markup "or the" g8 |
  f'4^\markup "gods are malign.'" f4. f4 b,8 |
  c4 c8 c c c4^\markup "Blame is" g8 |
  b4^\markup "better to" b4. b4 fis8 |
  bes4^\markup "give than receive." bes4. bes4 g8 |
  d'4.\3 a'4\2 d,8\3 d'4 |
  a8\2 a\2 d4 a8\2 a\2 d,4\3
  \bar "||"

  d'4.^\markup "You can choose..." cis8~ cis2 |
  a4.\2 b8\2~ b2\2 |
  e,4.\3 fis4\3 e8\3 a,4\3 |
  \time 3/4 a8 a a'\2 r a\2 r |
  \time 4/4
  d4.^\markup "If you choose..." cis8~ cis2 |
  b4.\2 a4\2 a8\2 e\3 a\2 |
  g4.\2 fis4\2 a8\2 a,4 |
  \time 3/4 r8 a'8\2 a16\2 a\2 e\3 e\3 a8\2 r |
  \time 4/4
  d4.^\markup "You can choose..." cis8~ cis2 |
  a4.\2 b8\2~ b2\2 |
  e,4.\3 fis4\3 a,8 e4 |
  \time 3/4 a8 a a'\2 r a\2 r |
  \time 4/4
  d4.^\markup "I will choose..." cis8~ cis2 |
  b4.\2 a4\2 a8\2 e\3 a\2 |
  g4.\2 a,8 r2 |
  d1\3 |

  \bar "||"
  \time 6/4 f,4 f8 c'4. b4 a g |
  \time 7/4 f4 f8 c'4 c8 e4 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 7/4 f4 f8 c'4 c8 e4 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 7/4 f4 f8 c'4 c8 e4 d b8 c g'4\2
  \bar "||"

  \time 6/4 f,4^\markup "There are those who think that" f8 c'4. b4 a g |
  \time 4/4 f4^\markup "they've been dealt a" f8 c'4 c8 e4 | d4^\markup "losing hand the" c d e |
  \time 6/4 f,4^\markup "cards were stacked against them" f8 c'4 c8 b4 a g |
  \time 7/4 f4^\markup "They weren't born in Lotusland." f8 c'4 c8 e4 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 7/4 f8 f f c'4 c8 e4 d b8 c g'4\2 |
  \time 6/4 f,4 f8 c'4 c8 b4 a g |
  \time 4/4 f4 f8 c'4 c8 e4 | d4 c d e |
  \bar "||"

  \time 4/4
  b4^\markup "All preordained," b4. b4 fis8 |
  bes4 bes4. bes4 g8^\markup "A" |
  f'4^\markup "prisoner in chains." f4. f4 b,8 |
  c4 c c8 c4 g8^\markup "A" |
  b4^\markup "victim of" b4. b4 fis8 |
  bes4 ^\markup "venemous" bes4. bes4 g8 |
  d'4.\3^\markup "fate." a'4\2 d,8\3 d' a\2 |
  r8 a\2 d a\2 r a\2 d,\3 a'\2 |

  b,4 b4.^\markup "Kicked in the face," b4 fis8 |
  bes4 bes4. bes4^\markup "you can" g8 |
  f'4^\markup "pray for a" f4. f4 b,8 |
  c4^\markup "place" c c8 c4 g8^\markup "in" |
  b4^\markup "heaven's" b4. b4 fis8^\markup "un-" |
  bes4 ^\markup "earthly" bes4. bes4 a8^\markup "e-" |
  d4\3^\markup "state." d8\3 a'16\2 a16\2 d,4\3 d'8 d,\3 |
  r8 d\3 r d16\3 d\3 a'\2 d,\3 r8 d' r    |

  d4.^\markup "You can choose..." cis8~ cis2 |
  a4.\2 b8\2~ b2\2 |
  e,4.\3 fis4\3 e8\3 a,4\3 |
  \time 3/4 a8 a a'\2 r a\2 r |
  \time 4/4
  d4.^\markup "If you choose..." cis8~ cis2 |
  b4.\2 a4\2 a8\2 e\3 a\2 |
  g4.\2 fis4\2 a8\2 a,4 |
  \time 3/4 \times 2/3 {g'8\2 a\2 a\2} \times 2/3 {g8\2 b a\2} g16\2 fis e\3 a |
  \time 4/4
  d4.^\markup "You can choose..." cis8~ cis2 |
  a4.\2 b8\2 r b\2 r b8\2 |
  e,4.\3 fis4\3 a8\2 a,4 |
  \time 3/4 a8 a a'\2 r a\2 r |
  \time 4/4
  d4.^\markup "I will choose..." cis8~ cis2 |
  b4.\2 a4\2 g8\2 fis e\2 |
  g,8 g g a\4 r2|

  \time 5/4
  d4\3 d8\3 a'4.\2 g4\2 fis |
  f,4 f8 c'4. bes4 a |
  bes4 bes8 f'4. ees4 d |
  ees4 ees8 bes'4. aes4 g |
  \time 4/4
  f8 e d c b a g f |
  \time 12/8
                                %\time 6/4
  g1.~ |
  g2.~ g2 b'8\3 a,
  \bar "||"
  d4\3 d\3 d8\3 a d\3 e, g fis a\4 f' |
  d4\3 d\3 d8\3 a d\2 e f e d c |
  f,4 f f8 e f g a bes a g |
  bes8 bes bes f' bes, f' ees bes'16 bes aes8 g f ees |

  d4\3 d8\3 a c cis d\3 f16 f d8\3 f a, c |
  cis8 d\3 f e g\2 f e d c b a g |
  f4 f f8 e f g a bes a g |
  bes8 bes bes f' bes, f' ees bes'16 bes ees,8 bes' ees, c |

  d4\3 d8\3 a c cis d\3 f16 f d8\3 g16\2 g\2 d8\3 gis16\2 gis\2 |
  d8\3 g16\2 g\2 d8\3 f d\3 des c a c cis d g, |
  f4 f f8 e f g a bes a g |
  bes8 bes bes f' bes, f' ees bes'16 bes aes8 g f ees |

  d4\3 d8\3 a c cis d\3 f16 f d8\3 f a, c |
  d8\3 a'16\2 a\2 d,8\3 aes'16\2 aes\2 d,8\3 g16\2 g\2 d8\3 f d\3 c a g |
  f4 f f8 e f g a bes a g |
  bes8 bes bes f'16 f bes,8 f' ees bes'16 bes ees,8 bes' ees, c |

  d4\3 d8\3 a c cis d\3 f16 f d\3 g\2 g\2 g\2 d\3 g\2 gis\2 gis\2 |
  dis16\3 dis\3 a'8\2 c a\2 aes\2 g\2 f d\3 des c a g |
  f4 f8 e f e f g a bes a g |
  a16 bes bes8 bes f' bes, f' ees bes' f c' g\2 d' |

  d,8\3 a d\3 a c cis d\3 f d\3 f8. a,16 c cis |
  d8\3 d' des,16 des des'8 c,16 c c'8 b, b' bes, bes' a, g |
  f4 f8 e f e f g a bes a g |
  bes8 bes16 bes bes8 f' f f ees ees16 ees ees8 bes'8 bes bes |

  \time 7/8
  bes8 [a] g [f] e [d c]
  \bar "||"

  \time 4/4
  b4 b4. b4 fis8 |
  bes4 bes4. bes4 g8 |
  f'4 f4. f4 b,8 |
  c4 c c8 c4 g8 |

  b4 b4. b4 fis8 |
  bes4 bes4. bes4 d16\3 d\3 |
  d4\3 d8\3 d16\3 d\3 fis8\3 a16\2 a\2 a4\2 |
  d16 d a8\2 d16 d a\2 a\2 g\2 ges\2 e\3 d\3 d' d8. |

  b,4 b4. b4 fis8 |
  bes4 bes4. bes4 g8 |
  f'4 f4. f4 b,8 |
  c4 c c8 c4 g8 |

  b4 b4. b4 fis8 |
  bes4 bes4. bes4 g8 |
  d'8\3 d\3 d\3 a'\2 d,\3 d\3 d' a\2 |
  a\2 d r a\2 d,16\3 d8.\3 d'8 a\2 |

  b,4 b4. b4 fis8 |
  bes4 bes4. bes4 g8 |
  f'4 f4. f4 b,8 |
  c4 c4. c4 g8 |

  b4 b4. b4 fis8 |
  bes4 bes4. bes4 bes8 |
  d4.\3 a'16\2 d,\3 d' d,\3 r8 d\3 d'16 a\2 |
  d16 d,\3 r8 d\3 r d'16 d,\3 r8 d4\3
  \bar "||"

  d'4. cis8~ cis2 |
  a4.\2 b8\2 r b\2 b\2 b\2 |
  e,4.\3 fis4\3 a8\2 a4\2 |
  \time 3/4 a4\2 a8\2 r a\2 r |

  \time 4/4 d4. cis8~ cis2 |
  b4.\2 a4.\2 e8\3 a\2 |
  g4.\2 fis4 a8\2 a,4 |
  \time 3/4 r8 a'8\2 a16\2 a\2 e\3 e\3 a8\2 r |

  \time 4/4 d4. cis8~ cis2 |
  a4.\2 b8\2 r b\2 r b\2 |
  e,4.\3 fis4\3 a8\2 a, r |
  \time 3/4 r8 a a'\2 r a\2 r |

  \time 4/4 d4. cis8~ cis2 |
  b4.\2 a4\2 g8\2 fis\2 e\3 |
  g,8 g g a\4 r2 |

  \time 5/4 d4\3 d8\3 a'4.\2 g4\2 fis\2 |
  f,4 f8 c'4. bes4 a |
  bes4 bes8 f'4. ees4 d |
  ees4 ees8 bes'4. aes4 g |

  \time 4/4 bes16 bes bes8 r4. bes,16 bes bes8 r8 |
  f'16 f f8 r4. f,16 f f8 r8 |
  g1~ | g1~ | g1 \bar "||"
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}

