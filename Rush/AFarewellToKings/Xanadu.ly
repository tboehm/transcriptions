\version "2.24.1"

%% Based on tabs from Sean Jones via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/aftk-xanadu.btab

% TODO:
% - Split into logical sections
% - Cue notes for synth bass parts

\header {
  title = "Xanadu"
  subtitle = "From 'A Farewell to Kings' by Rush (1977)"
  composer = "Geddy Lee"
}

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \key g \major

  \absolute {

    \time 4/4
    r1^\markup "Rest until 2:03"

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    e,2\4~ e,4.\4~ |
    e,2\4~ e,4.\4 |
    g2\2~ g4.\2~ |
    g2\2~ g4.\2 |
    d2\3~ d4.\3~ |
    d2\3~ d4.\3 |
    a,2\4~ a,4.\4~ |
    a,2.\4 g8\2 |
    e,2\4~ e,4.\4~ |
    e,2\4~ e,8\4 d8\3 e8\3 |
    g2\2~ g4.\2~ |
    g2\2 a8\2 g8\2 fis8\2 |
    d2\3~ d4.\3~ |
    d2\3 e8\3 d8\3 cis8\3 |
    a,2\4~ a,4.\4~ |
    a,2\4~ a,4.\4 |
    e,2\4~ e,4.\4~ |
    e,2\4~ e,4.\4 |
    g2\2~ g4.\2~ |
    g2\2~ g4.\2 |
    d2\3~ d4.\3~ |
    d2\3~ d4.\3 |
    a,2\4~ a,4.\4~ |
    a,2\4~ a,4.\4 |

    \time 4/4
    a,1\4~ |
    a,1\4~ |
    a,1\4~ |

    \time 5/4
    a,1\4~ a,4\4~ |

    \time 4/4
    a,2\4~ a,4.\4 e8\3~ |

    \repeat volta 3 {
      e8\3^\markup "3 times" e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
      e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
      e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 d'8\2~ |
      d'8\2 cis'8\2 b8\2 a8\3 fis'8\1 e'8\1 d'8\2 cis'8\2 |
      d'8\2 cis'8\2 b8\2 a8\3 cis'8\2 b8\2 a8\2 e8\3~ |
    }
    e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
    e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
    e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 d'8\2~ |
    d'8\2 cis'8\2 b8\2 a8\3 fis'8\1 e'8\1 d'8\2 cis'8\2 |
    e'8\1 d'8\2 cis'8\2 b8\2 d'8\2 cis'8\2 b8\2 a8\3 |
    cis'8\2 b8\2 a8\3 gis8\3 b8\2 a8\3 gis8\3 fis8\3 |
    a8\3 gis8\3 fis8\3 e8\3 gis8\3 fis8\3 e8\3 b,8\4 \glissando |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    e,2\4~ e,4.\4~ |
    e,2\4~ e,4.\4 |
    e,4\4 r4 r4. |
    R1*7/8*6 |

    r2 r8 e4\3 \glissando |
    e,4\4 e,4\4~ e,4.\4 |
    e,4\4 e,16\4 e,8.\4~ e,4\4 e8\3 \glissando |
    e,4\4 e,4\4~ e,4.\4 |
    e,4\4 e,4\4~ e,4.\4 |
    e,4\4 e,4\4~ e,4.\4 |
    e,4\4 e,16\4 e,8.\4~ e,4\4 b,8\3 \glissando |
    e4\3 e4\3 e8\3 b,8\4 e8\3 |
    d4\3 d4\3 d8\3 d16\3 d16\3 d8\3 |
    e4\3 e4\3 e8.\3 d8.\3 |
    e4\3 e4\3 e8.\3 b,16\4 e16\3 b,16\4 |
    d4\3 d4\3 d8.\3 a,16\4 d16\3 a,16\4 |
    d4\3 d8\3 a,8\4 d8\3 cis8\3 c8\3 |
    b,4\3 b,4\3 b,8.\3 a,8.\3 |
    b,4\3 cis8\3 d8\3 e8\3 fis8\2 \glissando b8\2 |
    a4\2 a4\2 a8\2 e8\3 a8\2 |
    g4\2 a8\2 g8\2 a8\2 b,8\4 a,8\4 |
    e4\3 e4\3 e8.\3 d8.\3 |
    e4\3 b,4\4 e8.\3 b,16\4 e16\3 b,16\4 |
    d4\3 d4\3 d8.\3 cis8.\3 |
    d4\3 d8\3 a,8\4 d8\3 cis8\3 c8\3 |
    b,4\3 b,4\3 b,8\3 fis,8\4 a,8\3 |
    b,4\3 cis8\3 d8\3 e8\3 fis8\2 \glissando b8\2 |

    \time 4/4
    a2\2~ a8\2 e8\3 a8\2 g8\2~ |
    g8\2 g8\2 e8\3 f8\3 fis4\3 d8\3 e8\3 |
    e,1\4 |
    r4. \hideNotes \grace {g16 \glissando} \unHideNotes e'4\1 d'8\1 \glissando b4\2 |
    g2\2~ g8\2 d8\3 g8\2 d8\3 |
    fis2\2 \glissando a2\2 |
    e,2.\4 r8 e'16\1 e'16\1 |
    fis'8\1 e'8\1 d'8\2 cis'8\2 e'16\1 d'16\2 cis'16\2 b16\2 a16\3 gis16\3 fis16\3 d16\4 |
    g2.\2 d8\3 g8\2 |
    fis2\2 \glissando b2\2 |
    e,1\4 |
    R1*3 |
    e2.\3 e8\3 e8\3 |
    r1 |
    e2.\3 e8\3 e8\3 |
    r1 |

    \repeat volta 4 {
      e4.\3^\markup "4 times" b,4\4 e8\3 fis4\3~ |
      fis8\3 fis8\3 b8\2 ais8\2 b8\2 ais8\2 fis8\3 b8\2 |
    }

    b4.\2 b8\2 fis8\3 b8\2 a4\2~ |
    a8\2 e8\3 e8\3 a8\2 gis8\2 a8\2 gis8\2 a8\2 |
    b4.\2 fis4\3 b8\2 a4\2~ |
    a8\2 e8\3 e8\3 a8\2 gis8\2 a8\2 b8\2 cis'8\1 |
    d'4\1 g8\2 gis8\2 a4\2 e8\3 a8\2 |
    g2\2 f2\3 |
    e,8\4 e,8\4 e,8\4 e,4\4 e,4\4 e,8\4 |
    e,4\4 r2. |

    \repeat volta 2 {
      e8\3 e8\3 b,8\4 e4\3 g4\2 e8\3 |
      a8\2 a8\2 e8\3 a4\2 g4\2 g8\2 |
      e4.\3 b4.\2 a4\2~ |

      \time 2/4
      a8\2 g4.\2 |

      \time 4/4
      a1\2 |
      e4.\3 b4.\2 a4\2~ |

      \time 2/4
      a8\2 g4.\2 \glissando |

      \time 4/4
      a,1\3 |
      e8\3 e8\3 b,8\4 e4\3 g4\2 e8\3 |
      a8\2 a8\2 e8\3 a4\2 g4\2 g8\2 |
      e1\3~ |
      e1\3 |
    }

    e2\3 g2\2 |
    d1\3 |
    R1*10 |

    d1\3~ |
    d1\3~ |
    d1\3~ |
    d1\3~ |
    d1\3 |
    r4 r16 d'16\1 d'8\1 e'16\1 d'16\1 b8\2 a16\2 g16\2 e8\3 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g4\2 fis8\2 e8\3 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c8\3 c8\3 g,8\4 c8\3 g8\2 g8\2 fis8\2 e8\3 |
    d8\3 d8\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g8\2 fis8\2 e8\3 g8\2 |
    d8\3 d8\3 r8 d8\3 \glissando ais,8\3 ais,8\3 r8 ais,8\3 |
    c8\3 c8\3 r8 c8\3 c'8\1 c'8\1 a8\2 g8\2 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g16\2 g16\2 fis8\2 e16\3 e16\3 g8\2 |
    d8\3 d8\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g8\2 fis8\2 e8\3 g8\2 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c8\3 g,8\4 c8\3 c8\3 g16\2 g16\2 fis8\2 e16\3 e16\3 g8\2 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g8\2 g8\2 fis8\2 e8\3 |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    d2\3~ d4.\3 |
    d2\3~ d4.\3 |

    \time 4/4
    d2\3~ d4.\3 e8\3~ |

    \time 4/4
    e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
    \repeat volta 3 {
      e8\3^\markup "3 times" e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
      e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 d'8\2~ |
      d'8\2 cis'8\2 b8\2 a8\3 fis'8\1 e'8\1 d'8\2 cis'8\2 |
      d'8\2 cis'8\2 b8\2 a8\3 cis'8\2 b8\2 a8\2 e8\3~ |
      e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
    }
    e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 e8\3~ |
    e8\3 e8\3 cis'8\2 b8\2 cis'8\2 e'8\1 cis'8\2 d'8\2~ |
    d'8\2 cis'8\2 b8\2 a8\3 fis'8\1 e'8\1 d'8\2 cis'8\2 |
    e'8\1 d'8\2 cis'8\2 b8\2 d'8\2 cis'8\2 b8\2 a8\3 |
    cis'8\2 b8\2 a8\3 gis8\3 b8\2 a8\3 gis8\3 fis8\3 |
    a8\3 gis8\3 fis8\3 e8\3 gis8\3 fis8\3 e8\3 b,8\4 \glissando \bar "||"

    e,8\4 e,8\4 e,8\4 e,4\4 e,4\4 e,8\4 |
    e,4\4 r2. |
    e8\3 e8\3 b,8\4 e4\3 g4\2 g8\2 |
    a8\2 a8\2 e8\3 a4\2 g4\2 g8\2 |
    e4\3 e8\3 b4\2 b8\2 a4\2 |

    \time 2/4
    a8\2 g4\2 g8\2 |

    \time 4/4
    a1\2 |
    e4\3 e8\3 b4\2 b8\2 a4\2 |

    \time 2/4
    a8\2 g4\2 g8\2 |

    \time 4/4
    a,1\3 |
    e8\3 e8\3 b,8\4 e4\3 g4\2 d8\3 |
    a8\2 a8\2 e8\3 a4\2 g4\2 g8\2 |
    e1\3~ |
    e1\3 |
    e8\3 e8\3 b,8\4 e4\3 g4\2 d8\3 |
    a8\2 a8\2 e8\3 a4\2 g4.\2 |
    e8\3 e8\3 e8\3 b8\2 b8\2 b8\2 a8\2 a8\2 |

    \time 2/4
    a8\2 g8\2 g8\2 g8\2 |

    \time 4/4
    a1\2 |
    e4\3 b,8\4 fis4\3 b,8\4 e4\3 |

    \time 2/4
    b,8\4 g4.\2 |

    \time 4/4
    a,1\3 |
    e8\3 e8\3 b,8\4 e4\3 g4\2 d8\3 |
    a8\2 a8\2 e8\3 a4\2 g4\2 g8\2 |
    e1\3~ |
    e1\3 |
    e2\3 g2\2 |
    d1\3 |
    R1*10 |

    d1\3~ |
    d1\3~ |
    d1\3~ \bar "||"
    d1\3~ |
    d1\3 |
    r8 g8\1\deadNote r16 d'16\1 d'8\1 e'16\1 d'16\1 b8\2 a16\2 g16\2 e8\3 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g8\2 g8\2 fis8\2 e8\3 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c8\3 c8\3 g,8\4 c8\3 g8\2 g8\2 fis8\2 e8\3 |
    d8\3 d8\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g8\2 g8\2 fis8\2 g8\2 |
    d8\3 d8\3 r8 d8\3 ais,8\3 ais,8\3 r8 ais,8\3 |
    c8\3 c8\3 r8 c8\3 c'8\1 c'8\1 b16\1 a16\2 g8\2 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g16\2 a16\2 g8\2 fis8\2 g8\2 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 c'8\1 c'8\1 b8\1 a8\2 |
    d4\3 a,8\4 d8\3 ais,4\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 g16\2 g16\2 fis8\2 fis16\2 fis16\2 g8\2 |
    d4\3 a,8\4 d8\3 ais,8\3 ais,8\3 f,8\4 ais,8\3 |
    c4\3 g,8\4 c8\3 c'8\1 c'8\1 b8\1 a8\2 |
    e1\3~ |
    e1\3~ |
    e1\3~ |
    e1\3 |
    e,4.\4 e,4.\4 e,4\4~ |
    e,1\4 |
    e,4.\4 e,4.\4 e,4\4~ |
    e,2.\4 e'4\1 \glissando |
    e4.\3 e8\3 b,4\4 fis4\3~ |
    fis8\3 fis8\3 b8\2 ais8\2 b8\2 ais8\2 fis8\3 b8\2 |
    e4.\3 b,4.\4 fis8\3 fis8\3~ |
    fis8\3 fis8\3 b8\2 ais8\2 cis'8\2 b8\2 ais8\2 fis8\3 |
    e4.\3 b,4\4 e8\3 fis4\3~ |
    fis8\3 fis8\3 b8\2 ais8\2 b8\2 ais8\2 fis8\3 b8\2 |
    e4.\3 b,4.\4 \times 2/3 {fis8\3 fis8\3 fis8\3} |
    \times 2/3 {cis'4\2 fis'4\1 cis'4\2} fis8.\3 cis'16\2 fis'16\1 cis'8.\2 |
    b4.\2 b8\2 fis8\3 b8\2 a4\2~ |
    a8\2 e8\3 e8\3 a8\2 gis8\2 a8\2 gis8\2 a8\2 |
    b4.\2 fis4\3 b8\2 a4\2~ |
    a8\2 e4\3 a8\2 gis8\2 a8\2 gis8\2 gis8\2 |
    e4.\3 b,4\4 e8\3 fis4\3~ |
    fis8\3 fis8\3 b8\2 ais8\2 b8\2 ais8\2 fis8\3 b8\2 |
    e4.\3 b,4\4 e8\3 fis8\3 fis8\3~ |
    fis8\3 b4\2 fis8\3 cis'8\2 b8\2 b8\2 fis8\3 |
    e4.\3 b,4\4 e8\3 fis4\3~ |
    fis8\3 fis8\3 b8\2 ais8\2 b8\2 ais8\2 fis8\3 b8\2 |
    e4.\3 b,4\4 e8\3 fis4\3~ |
    fis8\3 cis'4\2 fis8\3 fis'4\1 fis8\3 fis'8\1 |
    b4.\2 fis4\3 b8\2 a4\2~ |
    a8\2 e8\3 e8\3 a8\2 gis8\2 a8\2 gis8\2 a8\2 |
    b4.\2 fis4\3 b8\2 a4\2~ |
    a8\2 e4\3 a8\2 gis8\2 a8\2 b8\2 cis'8\1 |
    d'2\1 a2\2 |
    g2\2 f2\3 |

    \time 7/8
    \set Timing.beatStructure = 2,2,3
    \repeat volta 2 {
      e,2\4~ e,4.\4~ |
      e,2.\4 e,8\4 |
      e,2\4~ e,4.\4~ |
      e,2\4~ e,4.\4 |
    }

    \time 4/4
    e,1\4~ |
    e,1\4~ |
    e,1\4~ |
    e,1\4 |
    e,1\4~ |
    e,1\4~ |
    e,1\4 |
    e,1\4 |
    r1 |
    e,1\4 |
    e,1\4 |
    e,1\4 |
    e,1\4~ |
    e,1\4~ |
    e,1\4~ |
    e,1\4 |
    e,4\4 \times 2/3 {e,8\4 e,8\4 e,8\4} e4\3 \times 2/3 {e8\3 e8\3 e8\3} |
    e,4\4 r2. |
  }
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    <<
      \new Staff { \transpose c c, {\Bass} }
    >>
    \midi { }
  }
}
