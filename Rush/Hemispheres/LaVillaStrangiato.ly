\version "2.24.1"

%% Based on tabs from Andy Aledort, Sean Jones, and Dave Fornalsky via the Rush Tablature Project:
%% http://www.cygnusproductions.com/rtp/bass/hemi-la_villa_strangiato.btab

\include "repeatBracket.ly"

\header {
  title = "La Villa Strangiato"
  subtitle = "From 'Hemispheres' by Rush (1978)"
  composer = "Geddy Lee"
}

%% Frequently used grace notes
graceG = \acciaccatura { \parenthesize g16\2 }
graceC = \acciaccatura { \parenthesize c16\3 }

Bass = \relative c {
  \set Staff.instrumentName = #"Bass"
  \set Staff.midiInstrument = #"electric bass (finger)"
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = #2
  \numericTimeSignature

  \set Score.doubleRepeatBarType = #":|.|:"
  \set countPercentRepeats = ##t

  \key g \major
  \tempo 4 = 144

  \absolute {
    \time 4/4

    \repeatBracket 32 { r1 | }

    R1*3 |

    <<
      {
        R1*3 | \cueClefUnset
      }
      \new CueVoice {
        \cueClef "treble"
        r2 r4 r8 c'''16 b'' |
        c'''8 e''' d''' c'''~ c'''2~ |
        c'''1 |
      }
    >>


    \repeatBracket 4 { c8.\3^\markup "Bass drum enters" c8.\3 c8\3~ c2\3 | }
    g8.\2 g8.\2 g8\2~ g2\2 |
    g8.\2 g8.\2 g8\2~ g4\2 e4\2 |
    \repeatBracket 2 { c8.\3 c8.\3 c8\3~ c2\3 | }
    \repeatBracket 2 { g8.\2 g8.\2 g8\2~^\markup "Snare enters" g2\2 | }
    \repeatBracket 3 { c8.\3 c8.\3 c8\3~ c2\3 | }
    \repeatBracket 2 { c8.\3 c8.\3 c8\3~ c4\3 c'4\1 | }
    c8.\3 c8.\3 c8\3~ c8\3 ais16\1 b16\1 c'4\1 |
    c8.\3 c8.\3 c8\3~ c8\3 f16\2 fis16\2 g4\2 |
    c8.\3 c8.\3 c8\3~ c8\3 ais16\1 b16\1 c'8\1 f16\2 (g16\2) |
    c8.\3 c8.\3 c8\3~ c8\3 ais16\1 b16\1 c'4\1 |
    \repeatBracket 2 { c8.\3 c8.\3 c8\3~ c8\3 ais16\1 b16\1 c'8\1 f16\2 (g16\2) | }
    c8.\3 c8.\3 c8\3~ c8\3 ais16\1 b16\1 c'4\1 |
    \repeatBracket 6 { c8.\3 c8.\3 c8\3~ c8\3 ais16\1 b16\1 c'8\1 f16\2 (g16\2) | }

    \repeatBracket 3 {
      c8.\3 c8.\3 c8\3~ c2\3 |
    }

    c8.\3 c8.\3 c8\3~ c4.\3 \glissando g8\3 |

    \repeatBracket 4 {
      a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    }

    a,4.\3
    \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
    \mark \markup { \musicglyph #"scripts.segno" }
    a,8\3~ a,8\3 \graceG a8\2 g8\2 e8\3 |

    d4.\3 e4.\3 d4\3 |

    \repeatBracket 2 {
      a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    }

    a,4.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 e8\3 |
    d4.\3 e4.\3 d4\3 |
    a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    a,8\3 r16 a16\2 r16 a16\2 r16 a16\2 g8\2 g8\2 e8\3 d8\3 |

    %% "Chorus" 1
    c4\3 r8 c8\3~ c8\3 b,8\3 c8\3 g,8\4 |
    c8\3 c16\3 c16\3 b,8\3 \graceC d8\3~ d8\3 c8\3 b,8\3 a,8\4 |
    fis,4\4 r8 fis8\2 r16 fis16\2 fis16\2 cis16\3 fis8\2 cis8\3 |
    fis,4\4 fis,8\4 fis8\2 r16 fis16\2 fis16\2 cis16\3 fis8\2 cis8\3 |
    c4\3 r8 c8\3~ c8\3 b,8\3 b,16\3 \glissando c16\3 c16\3 g,16\4 |
    c8\3 c16\3 c16\3 b,8\3 \graceC d8\3~ d8\3 c8\3 b,8\3 a,8\4 |
    fis,4\4 r8 fis8\2 r16 fis16\2 fis16\2 cis16\3 fis8\2 cis8\3 |
    fis,8\4 fis16\2 fis16\2 r16 fis16\2 r16 fis16\2 f16\2 fis16\2 fis16\2 f16\2 cis8\3 e8\2 |

    \bar "||"

    %% "Verse" 2
    \repeatBracket 3 {
      a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    }
    a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 e8\3 |
    a,4.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 e8\3 |
    d4.\3 e4.\3 d4\3 |
    \repeatBracket 2 {
      a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    }
    a,4.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 e8\3 |
    d4.\3 e4.\3 d4\3 |
    a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    a,8\3 a16\2 a16\2 r16 a16\2 r16 a16\2 g16\2 a16\2 g8\2 e8\3 d8\3 |

    \bar "||"

    %% "Chorus" 2
    c4\3 r8 c8\3~ c8\3 b,8\3 c8\3 g,8\4 |
    c8\3 c16\3 c16\3 b,8\3 \graceC \glissando d8\3~ d8\3 c8\3 b,8\3 a,8\4 |
    fis,4\4 r8 fis8\2 r16 fis16\2 fis16\2 cis16\3 fis8\2 cis8\3 |
    fis,8.\4 fis,16\4 fis,8\4 fis16\2 fis16\2 r16 fis16\2 fis16\2 cis16\3 fis8\2 cis8\3 |

    c4\3 r8 c8\3~ c8\3 b,8\3 b,16\3 \glissando c16\3 c16\3 g,16\4 |
    c8\3 b,8\3 c8\3 d8\3~ d8\3 c8\3 b,8\3 a,8\4
    \once \override Score.RehearsalMark.font-size = #4
    \mark \markup { \musicglyph #"scripts.coda" } |
    fis,4\4 r8 fis8\2 r16 fis16\2 fis16\2 cis16\3 fis8\2 cis8\3 |
    fis,8\4 fis,16\4 fis,16\4~ fis,16\4 fis,16\4 fis16\2 fis16\2 r16 fis16\2 fis16\2 f16\2 fis8\2 f8\2 |

    \repeatBracket 3 {
      a,8.\3 a,8.\3 a,8\3~ a,8\3 \graceG a8\2 g8\2 a8\2 |
    }
    a,8.\3 a,8.\3 a,8\3 g16\2 (a16\2) g8\2 e8\3 d8\3 |

    \break

    %% Bridge to solo section
    %% TODO: I'd like to have a different bar type here ("||:"), but the repeatBracket prevents that.
    \time 7/8
    \set Timing.beatStructure = 2,2,3
    \key a \minor
    \repeatBracket 2 {
      f,2..\4~ |
      f,8\4 r8 f,8\4 r8 f,8\4 r4 |
    }

    \repeatBracket 2 {
      a,2..\4~ |
      a,8\4 r8 a,8\4 r8 a,8\4 r4 |
    }

    \repeatBracket 2 {
      f,2..\4~ |
      f,8\4 r8 f,8\4 r8 f,8\4 r4 |
    }

    \break

    %% Synth bass starts here
    %% TODO: https://lilypond.org/doc/v2.19/Documentation/notation/writing-parts#formatting-cue-notes

    \repeat volta 4 {
      a,2..\4~ |
      a,2..\4~ |
      a,2..\4~ |
      a,2..\4 |
    }
    \alternative {
      {
        f,2..\4~ ^\markup "Guitar solo starts 2nd time" |
        f,2..\4~ |
        f,2..\4~ |
        f,2..\4 |
      }
      {
        f,2..\4~ |
        f,2..\4 |
        f,2..\4~ |
        f,2..\4 |
      }
    }

    a,2..\4~ |
    a,2..\4  |
    R2..*2 |

    \bar "||"

    f,2..\4~ |
    f,2..\4 |
    f,2..\4~ |
    f,2..\4 |
    a,2..\4~ |
    a,2..\4  |
    a,2..\4~ |
    a,2..\4  |

    \bar "||"

    % ^\markup "(Bass guitar)"
    f,4\4 f,4\4 f,4\4 f,8\4 f,4\4 f,4\4 f,4\4 f,8\4 |
    f,4\4 f,4\4 f,8\4 f,8\4 f,8\4 f,8\4 r8 f,8\4 r8 f,8\4 f,8\4 f,8\4 |
    a,4\4 a,4\4 a,8\4 a,8\4 a,8\4 a,8\4 r8 a,4\4 a,8\4 a,8\4 a,8\4 |
    a,4\4 a,4\4 a,8\4 a,8\4 a,8\4 a,8\4 r8 a,8\4 r8 a,8\4 r8 a,8\4 |
    f,8\4 f,4\4 f,8\4 f,4\4 f,8\4 f,8\4 f,4\4 f,8\4 f,8\4 r8 f,8\4 |
    f,4\4 f,8\4 f,8\4 f,4\4 e,8\4 f,8\4 r8 f,4\4 f,8\4 f,8\4 g,8\4 |
    a,4\4 a,4\4 a,8\4 a,8\4 g,8\4 a,8\4 r4. a,8\4 a,8\4 g,8\4 |
    a,8\4 a,4\4 a,8\4 a,8\4 a,8\4 g,8\4 a,4.\4 a,8\4 a,8\4 a,8\4 g,8\4 |
    f,4\4 f,4\4 f,8\4 f,8\4 e,8\4 f,4\4 f,4\4 f,8\4 f,8\4 e,8\4 |
    f,8\4 f,8\4 f,4\4 f,8\4 f,8\4 e,8\4 f,4\4 f,8\4 f,8\4 f,8\4 f,8\4 f,8\4 |
    a,8\4 a,8\4 a,4\4 a,8\4 a,8\4 g,8\4 a,2\4 a,8\4 a,8\4 g,8\4 |
    a,4\4 a,8\4 a,8\4 a,8\4 a,8\4 g,8\4 a,2\4 a,8\4 a,8\4 g,8\4 |
    f,4\4 f,4\4 f,4\4 f,8\4 f,8\4 f,4.\4 f,8\4 f,8\4 f,8\4 |
    f,4\4 f,4\4 f,8\4 f,8\4 e,8\4 f,8\4 f,4.\4 f,8\4 f,8\4 f,8\4 |
    \repeatBracket 2 { a,8\4 a,4\4 a,8\4 a,8\4 a,8\4 g,8\4 a,8\4 a,4.\4 a,8\4 a,8\4 g,8\4 | }
    f,4\4 f,4\4 f,8\4 f,8\4 e,8\4 f,8\4 f,4.\4 f,8\4 f,8\4 f,8\4 |
    f,8\4 f,4\4 f,8\4 f,4.\4 f,8\4 f,4.\4 f,8\4 f,8\4 f,8\4 |
    a,2\4 a,8\4 a,8\4 g,8\4 a,8\4 a,4.\4 a,8\4 a,8\4 g,8\4 |
    a,8\4 a,4.\4 a,8\4 a,8\4 g,8\4 a,8\4 a,4.\4 a,8\4 a,8\4 g,8\4 |
    f,2\4 f,8\4 f,4\4 f,8\4 f,4.\4 f,4\4 f,8\4 |
    f,8\4 f,2\4~ f,8\4 f,8\4 f,8\4 f,4.\4 f,8\4 f,8\4 f,8\4 |

    \repeatBracket 2 { a,8\4 a,4\4 a,8\4 a,8\4 a,8\4 g,8\4 a,8\4 a,4.\4 a,8\4 a,8\4 g,8\4 | }

    %% It's not clear to me where the key change happens, but I think it's either here, 2 measures
    %% before or (unlikely) 4 measures later.
    \key g \major

    \repeatBracket 4 {
      a,8\4 r8 r4 r4. |
    }

    \time 4/4
    \repeatBracket 3 { a,8\3 e8\3 dis8\3 a,8\3 d16\3 c16\3 a,16\3 c16\3~ c16\3 a,16\3 b,8\3 | }
    r8 dis16\3 e16\3 dis16\3 d16\3 a,8\3 c8.\3 a,8.\3 r8 |
    \repeatBracket 3 { a,8\3 e8\3 dis8\3 a,8\3 d16\3 c16\3 a,16\3 c16\3~ c16\3 a,16\3 b,8\3 | }
    r8 dis16\3 e16\3 dis16\3 d16\3 a,8\3 c8.\3 a,16\3 r16 g,16\4 r16 g,16\4 |

    a,16\3 ^\markup "Bass solo" dis16\3 d16\3 dis16\3 a16\2 g16\2 e16\3 d'16\1 c'16\1 a16\2 dis'16\1 c'16\1 a16\2 d'16\1 c'16\1 a16\2 |
    c'16\1 cis'16\1 d16\2 c'16\1 a16\2 gis16\2 g16\2 e16\3 g16\2 g16\2 r16 g16\2~ g16\2 e16\3 g16\2 gis16\2 |
    a8.\2 gis8.\2 g8\2~ g8\2 fis8\2 f8\2 e8\2 |
    dis8.\2 d8.\2 cis8\3~ cis8\3 c8\3 b,8\3 ais,8\3 |
    a,8\3 r8 r8 g,8\4~ g,8\4 f,16\4 fis,16\4 g,8\4 f,16\4 fis,16\4 |
    g,8\4 f,16\4 fis,16\4 g,8\4 f,16\4 fis,16\4 g,8\4 g16\2 g16\2 r16 g,16\4 g,8\4 |
    c8\3 r8 r8 c8\3~ c2\3~ |
    c16\3 e16\3 d16\3 b,16\4 g16\2 g8\2 e16\3 d16\3 b,16\4 g16\2 g16\2~ g16\2 e16\3 g16\2 gis16\2 |
    a8.\2 gis8.\2 g8\2~ g8\2 fis8\2 f8\2 e8\2 |
    d8\2 r8 r4 r4 r8 a,8\4 |
    g,8\4 r8 r4 r2 |
    g,4\4 a,4\4 c4\3 d4\3 |
    e4\3 g4\2 a4\2 e4\3 |
    d8\3 r8 r4 r4 r8 r16 d16\3 |

    \time 2/4
    a,8\3 b,16\3 cis16\3 d16\2 f16\2 e16\2 d16\2 |

    \time 4/4
    c8\3 r8 r4 r4 r16 a16\2 a16\2 e16\3 |
    a8\2 r8 r4 r4 r16 e16\3 d16\3 b,16\4 |

    \time 3/4
    g16\2 g8\2 e16\3 d16\3 b,16\4 a16\2 a16\2~ a16\2 e16\3 d16\3 e16\3 |

    \time 4/4
    c16\3 r16 r16 c16\3 r8 c8\3 r8 e8\3 e16\3 e8.\3 |
    a,8\3 r8 r2. |
    % r1 |

    \time 4/4
    r1 |
    \time 2/4
    r2 |

    \bar "||"

    \time 4/4
    c4\3 ^\markup "Guitar solo 2" c8.\3 g,16\4 b,8\3 c8\3~ c8\3 g,8\4 |
    c4\3 c8\3 r16 g,16\4 fis,8\4 fis,8\4 r4 |
    fis4\2 fis8\2 e,16\4 f,16\4 fis,8\4 fis,8\4 r4 |
    fis8\2 f8\2 e8\2 a,16\3 ais,16\3 b,8\3 c8\3 r8 g,8\4 |
    c4\3 c8.\3 g,16\4 b,8\3 c8\3~ c8\3 g,8\4 |
    d8\3 c8\3 b,8\3 d8\3 fis,8\4 fis,8\4 r8 d8\2 |
    fis8\2 f8\2 fis8\2 a,16\3 ais,16\3 fis,8\4 fis,8\4 r4 |
    fis8\2 cis8\3 fis8\2 b,8\3 c8\3 c8\3 r8 g,8\4 |
    c4\3 c8.\3 g,16\4 b,8\3 c8\3~ c8\3 g,8\4 |
    c8\3 d8\3 c8\3 a,8\3 fis,8\4 fis,8\4 r8 fis8\2 |
    fis4\2 fis8\2 a,16\3 a,16\3 fis,8\4 fis,8\4 r8 fis,8\4 |
    fis8\2 f8\2 fis8\2 c16\3 c16\3 b,8\3 c8\3 r8 g,8\4 |
    c4\3 c8.\3 g,16\4 b,8\3 c8\3~ c8\3 g,8\4 |
    d8\3 c8\3 b,8\3 d16\3 a,16\3 fis,8\4 fis,8\4 r8 fis8\2 |
    fis4\2 fis8.\2 a,16\3 fis,8\4 fis,8\4 r4 |
    fis8\2 f8\2 fis8\2 a,16\3 b,16\3 c8\3 b,8\3 r8 d8\3 |
    c4\3 e8\3 d8\3 r8 c8\3 r8 b,8\3 |
    c8\3 d8\3 c8\3 b,8\3 fis,8\4 fis,8\4 r4 |
    fis4\2 fis8.\2 a,16\3 fis,8\4 fis,8\4 r4 |
    fis8\2 f8\2 fis4\2 c8\3 b,8\3 r8 d8\3 |
    c4\3 e8\3 d8\3 r8 c8\3 r8 b,8\3 |
    c8\3 d8\3 c8\3 a,8\4 fis,8\4 fis,8\4 r4 |
    fis4\2 fis8.\2 a,16\3 fis,8\4 fis,8\4 r4 |

    \time 2/4
    \key c \major

    fis8\2 f8\2 fis8\2 cis8\3 |

    \time 9/8
    a4\2 r8 e4\3 r8 a,4\3 r8 |
    g4\2 r8 d4\3 r8 g,4\4 r8
    cis'4\1 r8 a4\2 r8 e4\3 r8 |
    a8\2 e8\3 a,8\3 g8\2 d8\3 a,8\3 cis'8\1 a8\2 e8\3 |
    d8\3 cis8\3 d8\3 e8\3 d8\3 cis8\3 b,8\4 ais,8\4 b,8\4 |
    a8\2 e8\3 a,8\3 g8\2 d8\3 a,8\3 cis'8\1 a8\2 e8\3 |
    r8 cis8\3 d8\3 e8\3 d8\3 cis8\3 g,8\4 fis,8\4 g,8\4 |
    a8\2 e8\3 a,8\3 g8\2 d8\3 a,8\3 cis'8\1 a8\2 e8\3 |
    r16 cis16\3 d8\3 d8\3 r16 d16\3 e8\3 e8\3 r16 b,16\4 b,8\4 b,8\4 |
    a8\2 e8\3 a,8\3 g8\2 d8\3 a,8\3 cis'8\1 a8\2 e8\3 |
    r16 cis16\3 d8\3 d8\3 r16 d16\3 e8\3 e8\3 r16 g,16\4 g,8\4 g,8\4 |
    r16 g,16\4 g,8\4 g,8\4 r16 g,16\4 g,8\4 g,8\4 r16 g,16\4 g,8\4 g,8\4 |

    \time 12/8
    a,8\3 [r8 a,8\3] a,8\3 [r8 a,8\3] a,8\3 [r8 a,8\3] a,8\3 [r8 a,8\3] |
    \repeatBracket 3 { a,4\3 e8\3 dis4\3 a,8\3 d4\3 c8\3 a,4\3 b,8\3 | }

    \time 4/4
    r8 dis16\3 e16\3 dis16\3 d16\3 a,8\3 c8.\3 a,16\3~ a,8\3 r8 |
    \repeatBracket 3 { a,8\3 e8\3 dis8\3 a,8\3 d16\3 c16\3 a,16\3 c16\3~ c16\3 a,16\3 b,8\3 | }
    r8 dis16\3 e16\3 dis16\3 d16\3 a,8\3 c8.\3 a,16\3~ a,8\3 r8 |
    \repeatBracket 3 { a,8\3 e8\3 dis8\3 a,8\3 d16\3 c16\3 a,16\3 c16\3~ c16\3 a,16\3 b,8\3 | }
    r8 dis16\3 e16\3 dis16\3 d16\3 a,8\3 c8.\3 a,16\3~ a,8\3 r8 |
    c8.\3 c8.\3 c8\3~ c2\3 |
    c8.\3 c8.\3 c8\3~ c4.\3 ais8\3 |
    \repeatBracket 2 { a,8.\3 a,8.\3 a,8\3~ a,8\3 a8\2 g8\2 a8\2 | }

    %% \cadenzaOn will suppress the bar count and \stopStaff removes the staff lines.
    \cadenzaOn
    \stopStaff
    %% Some examples of possible text-displays

    %% text line-aligned
    %% ==================
    %% Move text to the desired position
    %% \once \override TextScript.extra-offset = #'( 2 . -3.5 )
    %% | <>^\markup { D.S. al Coda } }

    %% text center-aligned
    %% ====================
    %% Move text to the desired position
    %% \once \override TextScript.extra-offset = #'( 6 . -5.0 )
    %% | <>^\markup { \center-column { D.S. "al Coda" } }

    %% text and symbols center-aligned
    %% ===============================
    %% Move text to the desired position and tweak spacing for optimum text alignment
    \repeat unfold 1 {
      s1
      \bar ""
    }

    \once \override TextScript.extra-offset = #'( 0 . -3.0 )
    \once \override TextScript.word-space = #1.5
    <>^\markup { \center-column { "D.S. al Coda" \line { \musicglyph #"scripts.coda" \musicglyph #"scripts.tenuto" \musicglyph #"scripts.coda"} } }

    %% Increasing the unfold counter will expand the staff-free space
    \repeat unfold 3 {
      s1
      \bar ""
    }
    %% Resume bar count and show staff lines again
    \startStaff
    \cadenzaOff

    %% Coda on a new line
    \break

    % %% Show up, you clef and key!
    % \once \override Staff.KeySignature.break-visibility = #end-of-line-invisible
    % \once \override Staff.Clef.break-visibility = #end-of-line-invisible

    %% The coda
    \repeatBracket 2 {
      fis,8.\4

      %% Coda on new line, use this:
      \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 0 )
      \once \override Score.RehearsalMark.font-size = #5
      \mark \markup { \musicglyph #"scripts.coda" }

      fis,8.\4 fis,8\4 r2 |
    }

    \time 7/8
    \repeatBracket 3 { a,4\4 r2 r8 | }
    a,4\4 r2 e8\2 |

    \time 4/4
    a8.\2 gis8.\2 g8\2~ g8\2 fis8\2 f8\2 e8\2 |
    dis8.\2 d8.\2 cis8\3~ cis8\3 c8\3 b,8\3 ais,8\3 |
    a,16\3 e16\3 d16\3 b,16\4 g16\2 g8\2 e16\3 d16\3 b,16\4 g16\2 g16\2~ g16\2 e16\3 d16\3 e16\3 |
    c8\3 r8 r4 r2 |

  }
}

\book {
  \score {
    <<
      \new Staff {
        \omit Voice.StringNumber
        \clef bass
        << \Bass >>
      }
      \new TabStaff \with { stringTunings = #bass-tuning } {
        << \transpose c c, {\Bass} >>
      }
    >>
  }

  \score {
    \unfoldRepeats {
      <<
        \new Staff { \transpose c c, {\Bass} }
      >>
    }
    \midi { }
  }
}
